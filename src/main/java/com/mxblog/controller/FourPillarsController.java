package com.mxblog.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.mxblog.service.FourPillarsService;
import com.mxblog.vo.FourPillarsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class FourPillarsController {
	@Autowired
	FourPillarsService fourPillarsService;

	@PostMapping("/getFourPillarsVOByDateAndTime")
	@ResponseBody
	public String getFourPillarsVOByDateAndTime(String dateString) {
		FourPillarsVO vo = fourPillarsService.getFourPillarsVO(dateString);
		// 防止产生 $ref
		return JSON.toJSONString(vo, SerializerFeature.DisableCircularReferenceDetect);
	}

	@PostMapping("/getFourPillarsVOByCode")
	@ResponseBody
	public String getFourPillarsVOByCode(String code) {
		code = fourPillarsService.subCode(code);
		String dateString = fourPillarsService.getDateAndTime(code);
		FourPillarsVO vo = fourPillarsService.getFourPillarsVO(dateString);
		// 防止产生 $ref
		return JSON.toJSONString(vo, SerializerFeature.DisableCircularReferenceDetect);
	}

	@RequestMapping("/checkDateAndTime")
	@ResponseBody
	public String checkDateAndTime(String date, String time) {
		if (!date.matches("[0-9]{4}-[0-9]{2}-[0-9]{2}")) {
			return "error";
		}
		if (!time.matches("[0-9]{2}:[0-9]{2}")) {
			return "error";
		}
		return "success";
	}

	@RequestMapping("/checkPillarCode")
	@ResponseBody
	public String checkPillarCode(String code) {
		if (code.matches("FP-[A-J]{4}M[A-J]{2}M[A-J]{2}S[A-J]{2}O[A-J]{2}O[A-J]{2}")) {
			return "success";
		}
		return "error";
	}
}
