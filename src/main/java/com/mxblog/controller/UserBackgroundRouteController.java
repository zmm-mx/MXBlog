package com.mxblog.controller;

import com.alibaba.fastjson.JSON;
import com.mxblog.pojo.User;
import com.mxblog.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/background")
public class UserBackgroundRouteController {
    @Autowired
    private UserServiceImpl userService;
    @Value("${pageInfo.pageSize}")
    private int pageSize;
    @Value("${file.uploadFolder}")
    private String uploadFolder;

    @GetMapping("/toUserManage")
    public String toMyGame(){
        return "background/userManage";
    }

    @GetMapping("/getUsers")
    @ResponseBody
    public String getUsers(int startIndex){
        HashMap<String, Object> map = new HashMap<>();
        map.put("startIndex", (startIndex-1)*pageSize);
        map.put("pageSize", pageSize);
        List<User> users = userService.queryUserByPage(map);
        String userString = JSON.toJSONString(users);
        return userString;
    }

    @GetMapping("/getUserPageSum")
    @ResponseBody
    public String getUserPageSum(){
        int number = userService.countUsers();
        int sum = (int) Math.ceil((double) number/(double) pageSize);
        return ""+sum;
    }

    @GetMapping("/clearUserAvatar/{id}")
    @ResponseBody
    public String clearUserAvatar(@PathVariable int id) throws IOException {
        File realPath = new File(uploadFolder+"avatar/");
        if (!realPath.exists()){
            realPath.mkdir();
        }
        User user = userService.queryUserById(id);
        FileChannel inputChannel = null;
        FileChannel outputChannel = null;
        try {
            inputChannel = new FileInputStream(new File(this.getClass().getResource("/static/images/avatar-default.jpg").getPath())).getChannel();
            outputChannel = new FileOutputStream(new File(uploadFolder + "avatar/", user.getId() + ".jpg")).getChannel();
            outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
            userService.changeUserAvatarTypeToJPG(id);
            inputChannel.close();
            outputChannel.close();
        } finally {

        }
        return "";
    }

    @GetMapping("/clearUserUsername/{id}")
    @ResponseBody
    public String clearUserUsername(@PathVariable int id){
        HashMap<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("username", "uid_"+id);
        userService.clearUsernameById(map);
        return "";
    }
}
