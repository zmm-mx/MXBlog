package com.mxblog.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.mxblog.service.PlumBlossomService;
import com.mxblog.utils.CharacterUtils;
import com.mxblog.vo.PlumBlossomVO;
import com.mxblog.vo.RandomCharactersVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 梅花易数的控制层
 * @author 妙霄
 */
@Controller
public class PlumBlossomController {
	@Autowired
	private PlumBlossomService plumBlossomService;

	@PostMapping("/getPlumBlossomVOByCharacters")
	@ResponseBody
	public String getPlumBlossomVOByCharacters(String ch1, String ch2) throws UnsupportedEncodingException {
		Date date = new Date();
		// 大写的 HH 是 24 小时制，如果用 hh 就是 12 小时制
		DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = dateformat.format(date);
		PlumBlossomVO vo = plumBlossomService.getPlumBlossomVO(ch1, ch2, dateString);
		// 防止产生 $ref
		return JSON.toJSONString(vo, SerializerFeature.DisableCircularReferenceDetect);
	}

	@PostMapping("/getPlumBlossomVOByCode")
	@ResponseBody
	public String getPlumBlossomVOByCode(String code) throws UnsupportedEncodingException {
		code = plumBlossomService.subCode(code);
		List<String> strList = plumBlossomService.getCharactersAndTime(code);
		PlumBlossomVO vo = plumBlossomService.getPlumBlossomVO(strList.get(0), strList.get(1), strList.get(2));
		// 防止产生 $ref
		return JSON.toJSONString(vo, SerializerFeature.DisableCircularReferenceDetect);
	}

	@GetMapping("/getRandomCharacters")
	@ResponseBody
	public RandomCharactersVO getRandomCharacters() throws UnsupportedEncodingException {
		RandomCharactersVO vo = new RandomCharactersVO();
		byte[] bytes1 = new byte[2];
		byte[] bytes2 = new byte[2];
		bytes1[0] = (byte)(0xa0 + (int) (Math.random() * 40) + 16);
		bytes1[1] = (byte)(0xa0 + (int) (Math.random() * 94));
		bytes2[0] = (byte)(0xa0 + (int) (Math.random() * 40) + 16);
		bytes2[1] = (byte)(0xa0 + (int) (Math.random() * 94));
		vo.setCh1(new String(bytes1, "GBK"));
		vo.setCh2(new String(bytes2, "GBK"));
		return vo;
	}

	@RequestMapping("/checkCharacters")
	@ResponseBody
	public String checkCharacters(String ch1, String ch2) throws UnsupportedEncodingException {
		int strokes1 = CharacterUtils.calculateNumberOfStrokes(ch1.getBytes("GBK"));
		int strokes2 = CharacterUtils.calculateNumberOfStrokes(ch2.getBytes("GBK"));
		if (strokes1 < 1 || strokes2 < 1) {
			return "error";
		}
		return "success";
	}

	@RequestMapping("/checkDiagramCode")
	@ResponseBody
	public String checkDiagramCode(String code) {
		if (code.matches("(PB-)?M?[A-Z]{1,3}TM?[A-Z]{1,3}KM?[A-Z]{1,3}TM?[A-Z]{1,3}K[A-J]{4}M[A-J]{2}M[A-J]{2}S[A-J]{2}O[A-J]{2}O[A-J]{2}")) {
			return "success";
		}
		return "error";
	}
}
