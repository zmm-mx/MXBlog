package com.mxblog.controller;

import com.alibaba.fastjson.JSON;
import com.mxblog.pojo.Blog;
import com.mxblog.pojo.BlogColumn;
import com.mxblog.pojo.BlogTag;
import com.mxblog.service.BlogColumnServiceImpl;
import com.mxblog.service.BlogServiceImpl;
import com.mxblog.service.BlogTagServiceImpl;
import com.mxblog.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

@Controller
public class IndexRouteController {
    @Autowired
    private BlogServiceImpl blogService;
    @Autowired
    private BlogColumnServiceImpl blogColumnService;
    @Autowired
    private BlogTagServiceImpl blogTagService;
    @Autowired
    private RedisUtil redisUtil;

    @Value("${pageInfo.pageSize}")
    private int pageSize;

    @PostMapping("/getBlogsWithoutShieldAndNotTop")
    @ResponseBody
    public String getBlogsWithoutShieldAndNotTop(String startIndex, String topsNum){
        HashMap<String, Object> map = new HashMap<>();
        int startIndexNum = Integer.parseInt(startIndex);
        int topsNumber = Integer.parseInt(topsNum);
        map.put("startIndex", Math.max(0, (startIndexNum-1)*pageSize-topsNumber));
        if (startIndexNum<=(topsNumber/10+1)){
            map.put("pageSize", Math.max(0, pageSize-(topsNumber%10)));
        } else {
            map.put("pageSize", pageSize);
        }
        List<Blog> blogs = blogService.queryBlogByPageWithoutShieldAndNotTop(map);
        // 读取Redis设置浏览量
        for (Blog blog : blogs) {
            Object views = redisUtil.get(blog.getRedisKeyWithId());
            if (views != null) {
                blog.setViews((Integer) views);
            }
            else {
                blog.setViews(0);
            }
        }
        String blogString = JSON.toJSONString(blogs);
        return blogString;
    }
    @PostMapping("/getBlogPageSumWithoutShield")
    @ResponseBody
    public String getBlogPageSumWithoutShield(){
        int number = blogService.countBlogWithoutShield();
        int sum = (int) Math.ceil((double) number/(double) pageSize);
        return ""+sum;
    }
    @GetMapping("/getBlogCountWithoutShield")
    @ResponseBody
    public String getBlogCountWithoutShield(){
        int number = blogService.countBlogWithoutShield();
        return ""+number;
    }
    @GetMapping("/getBlogColumnsByPage")
    @ResponseBody
    public String getBlogColumnsByPage(int startIndex){
        HashMap<String, Integer> map = new HashMap<>();
        map.put("startIndex", (startIndex-1)*pageSize);
        map.put("pageSize", pageSize);
        List<BlogColumn> blogColumns = blogColumnService.queryBlogColumnByPage(map);
        for (BlogColumn blogColumn : blogColumns) {
            blogColumn.setBlogSum(blogService.queryBlogSumByColumnIdWithoutShield(blogColumn.getId()));
        }
        String columnString = JSON.toJSONString(blogColumns);
        return columnString;
    }
    @GetMapping("/getBlogTagsByPage")
    @ResponseBody
    public String getBlogTagsByPage(int startIndex){
        HashMap<String, Integer> map = new HashMap<>();
        map.put("startIndex", (startIndex-1)*pageSize);
        map.put("pageSize", pageSize);
        List<BlogTag> blogTags = blogTagService.queryBlogTagByPage(map);
        for (BlogTag blogTag : blogTags) {
            blogTag.setBlogSum(blogService.queryBlogSumByTagIdWithoutShield(blogTag.getId()));
        }
        String tagString = JSON.toJSONString(blogTags);
        return tagString;
    }
    @GetMapping("/getBlogsByViewsWithoutShield")
    @ResponseBody
    public String getBlogsByViewsWithoutShield(String startIndex){
        //MySQL 直接操作
        /*HashMap<String, Object> map = new HashMap<>();
        int startIndexNum = Integer.parseInt(startIndex);
        map.put("startIndex", (startIndexNum-1)*pageSize);
        map.put("pageSize", pageSize);
        System.out.println((startIndexNum-1)*pageSize);
        List<Blog> blogs = blogService.getBlogsByViewsWithoutShield(map);*/
        //改用Redis操作
        Set newSet = new LinkedHashSet();
        for(int i = pageSize, last = -1;i>0;) {
            if (last>=redisUtil.ZCard("mxblog:rank:view")) {
                break;
            }
            Set set = redisUtil.ZRevRange("mxblog:rank:view", last+1, last+pageSize);
            Set removeSet = new LinkedHashSet();
            for (Object id : set) {
                if (i<=0) {
                    removeSet.add(id);
                }
                else {
                    if (blogService.queryShieldByBlogId((int)id)==-1) {
                        removeSet.add(id);
                    } else {
                        i--;
                    }
                }
            }
            set.removeAll(removeSet);
            newSet.addAll(set);
            removeSet.clear();
            last = last + pageSize;
        }
        ArrayList<Blog> blogs = new ArrayList<>();
        for (Object id : newSet) {
            blogs.add(blogService.queryBlogTitleAndIdByBlogId((int)id));
        }
        String blogString = JSON.toJSONString(blogs);
        return blogString;
    }
    @GetMapping("/getTopBlogWithoutShield")
    @ResponseBody
    public String getTopBlogWithoutShield(){
        List<Blog> blogs = blogService.getTopBlogWithoutShield();
        // 读取Redis设置浏览量
        for (Blog blog : blogs) {
            Object views = redisUtil.get(blog.getRedisKeyWithId());
            if (views != null) {
                blog.setViews((Integer) views);
            }
            else {
                blog.setViews(0);
            }
        }
        String blogString = JSON.toJSONString(blogs);
        return blogString;
    }
}
