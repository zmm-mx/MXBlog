package com.mxblog.controller;

import com.alibaba.fastjson.JSON;
import com.mxblog.pojo.Archives;
import com.mxblog.service.ArchivesServiceImpl;
import com.mxblog.service.BlogServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ArchivesRouteController {
    @Autowired
    private ArchivesServiceImpl archivesService;
    @Autowired
    private BlogServiceImpl blogService;

    @GetMapping("/getArchivesWithoutShield")
    @ResponseBody
    public String getArchivesWithoutShield(){
        List<String> years = archivesService.queryAllYearsWithoutShield();
        List<Archives> archives = new ArrayList<>();
        for (String year : years) {
            List<Archives> archivesByYear = archivesService.queryBlogsByYearWithoutShield(year);
            for (Archives archive : archivesByYear) {
                archives.add(archive);
            }
        }
        String archivesString = JSON.toJSONString(archives);
        return archivesString;
    }
}
