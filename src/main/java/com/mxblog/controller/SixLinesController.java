package com.mxblog.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.mxblog.service.SixLinesService;
import com.mxblog.vo.PlumBlossomVO;
import com.mxblog.vo.SixLinesVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class SixLinesController {
	@Autowired
	private SixLinesService sixLinesService;

	@PostMapping("/getSixLinesVOByLines")
	@ResponseBody
	public String getSixLinesVOByLines(@RequestParam("lines") int[] lines) {
		Date date = new Date();
		// 大写的 HH 是 24 小时制，如果用 hh 就是 12 小时制
		DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = dateformat.format(date);
		SixLinesVO vo = sixLinesService.getSixLinesVO(lines, dateString);
		// 防止产生 $ref
		return JSON.toJSONString(vo, SerializerFeature.DisableCircularReferenceDetect);
	}

	@PostMapping("/getSixLinesVOByCode")
	@ResponseBody
	public String getSixLinesVOByCode(String code) {
		code = sixLinesService.subCode(code);
		String[] paramList = sixLinesService.getLineAndDateTime(code);
		int[] lines = new int[6];
		for (int i = 0; i < 6; i++) {
			lines[i] = Integer.parseInt(paramList[i]);
		}
		String dateString = paramList[6];
		SixLinesVO vo = sixLinesService.getSixLinesVO(lines, dateString);
		// 防止产生 $ref
		return JSON.toJSONString(vo, SerializerFeature.DisableCircularReferenceDetect);
	}

	@GetMapping("/getRandomLines")
	@ResponseBody
	public SixLinesVO getRandomLines(){
		SixLinesVO vo = new SixLinesVO();
		int[] linesArray = new int[6];
		for (int i = 0; i < linesArray.length; i++) {
			linesArray[i] = (int) (Math.random() * 4);
		}
		vo.setLinesArray(linesArray);
		return vo;
	}

	@RequestMapping("/checkLines")
	@ResponseBody
	public String checkCharacters(@RequestParam("lines") int[] lines) {
		for (int line : lines) {
			if (line < 0 || line > 3) {
				return "error";
			}
		}
		return "success";
	}

	@RequestMapping("/checkLineCode")
	@ResponseBody
	public String checkLineCode(String code) {
		if (code.matches("SL-[A-D]{6}[A-J]{4}M[A-J]{2}M[A-J]{2}S[A-J]{2}O[A-J]{2}O[A-J]{2}")) {
			return "success";
		}
		return "error";
	}
}
