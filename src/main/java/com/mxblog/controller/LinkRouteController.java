package com.mxblog.controller;

import com.alibaba.fastjson.JSON;
import com.mxblog.pojo.Link;
import com.mxblog.service.LinkServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

@Controller
public class LinkRouteController {
    @Autowired
    private LinkServiceImpl linkService;

    @Value("${pageInfo.pageSize}")
    private int pageSize;

    @GetMapping("/getLinksByPage")
    @ResponseBody
    public String getLinksByPage(int startIndex){
        HashMap<String, Integer> map = new HashMap<>();
        map.put("startIndex", (startIndex-1)*pageSize);
        map.put("pageSize", pageSize);
        List<Link> links = linkService.queryLinkByPage(map);
        String linkString = JSON.toJSONString(links);
        return linkString;
    }

    @GetMapping("/getLinkCount")
    @ResponseBody
    public String getLinkCount(){
        int number = linkService.countLink();
        return ""+number;
    }

    @GetMapping("/getLinkPageSum")
    @ResponseBody
    public String getLinkPageSum(){
        int number = linkService.countLink();
        int sum = (int) Math.ceil((double) number/(double) pageSize);
        return ""+sum;
    }

}
