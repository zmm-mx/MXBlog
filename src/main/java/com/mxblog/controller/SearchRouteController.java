package com.mxblog.controller;

import com.alibaba.fastjson.JSON;
import com.mxblog.pojo.Blog;
import com.mxblog.service.SearchServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class SearchRouteController {
    @Autowired
    private SearchServiceImpl searchService;
    @Value("${pageInfo.pageSize}")
    private int pageSize;

    @PostMapping("/findBlogsByTitleWithPage")
    @ResponseBody
    public String findBlogsByTitleWithPage(String title, String startIndex){
        int from = Integer.parseInt(startIndex);
        List<Blog> blogs = searchService.findBlogByTitleWithPage(title, from, pageSize);
        String blogString = JSON.toJSONString(blogs);
        return blogString;
    }
}
