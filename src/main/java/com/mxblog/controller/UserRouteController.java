package com.mxblog.controller;

import com.mxblog.pojo.User;
import com.mxblog.service.UserServiceImpl;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.Date;

@Controller
public class UserRouteController {
    @Autowired
    private UserServiceImpl userService;
    @Value("${file.uploadFolder}")
    private String uploadFolder;

    @GetMapping("/toLogin")
    public String toLogin(){
        return "front/login";
    }
    @PostMapping("/login")
    public String login(String email, String password, HttpSession session, Model model){
        //获取当前用户
        Subject subject = SecurityUtils.getSubject();
        //封装用户的登录数据
        UsernamePasswordToken token = new UsernamePasswordToken(email, password);
        //执行登录的方法
        try {
            subject.login(token);
            User user = (User) SecurityUtils.getSubject().getPrincipal();
            session.setAttribute("userIdSession", user.getId());
            session.setAttribute("userNameSession", user.getUsername());
            session.setAttribute("avatarTypeSession", user.getAvatarType());
            return "redirect:index";
        }catch (UnknownAccountException e){
            model.addAttribute("msg", "该邮箱未被注册！");
            return "front/login";
        }catch (IncorrectCredentialsException e) {
            model.addAttribute("msg", "密码错误！");
            return "front/login";
        }
    }
    @GetMapping("/logout")
    public String logout(){
        //获取当前用户
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return "redirect:/";
    }
    @GetMapping("/noauth")
    public String unauthorized(){
        return "error/noauth";
    }
    @GetMapping("/toRegist")
    public String toRegist(){
        return "front/regist";
    }
    @PostMapping("/regist")
    public String regist(User user, @RequestParam("avatar") MultipartFile file, HttpSession session, Model model) throws IOException {
        //设置权限
        user.setAuthority("normal");
        //保留旧密码
        String oldPWD = user.getPassword();
        //盐值散列加密
        user.setPassword(new Md5Hash(user.getPassword(), user.getUsername(), 1024).toHex());
        //上传路径保存设置
        File realPath = new File(uploadFolder+"avatar/");
        if (!realPath.exists()){
            realPath.mkdir();
        }
        //获取文件原名
        String fileName = file.getOriginalFilename();
        //获取文件后缀
        String fileType = fileName.substring(fileName.lastIndexOf(".") + 1);
        //设置文件后缀
        user.setAvatarType(fileType);
        //设置创建时间
        user.setCreateTime(new Date());
        //在这一句中，已经把插入后的id赋值给user了，后面可以直接取
        if(userService.insertUser(user)>0){
            //通过CommonsMultipartFile的方法直接写文件
            file.transferTo(new File(uploadFolder+"avatar/", user.getId()+"."+user.getAvatarType()));
            //直接执行登录代码
            //获取当前用户
            Subject subject = SecurityUtils.getSubject();
            //封装用户的登录数据
            UsernamePasswordToken token = new UsernamePasswordToken(user.getEmail(), oldPWD);
            //执行登录的方法
            try {
                subject.login(token);
                session.setAttribute("userIdSession", user.getId());
                session.setAttribute("userNameSession", user.getUsername());
                session.setAttribute("avatarTypeSession", user.getAvatarType());
                return "redirect:/";
            }catch (UnknownAccountException e){
                model.addAttribute("msg", "用户名不存在！");
                return "front/login";
            }catch (IncorrectCredentialsException e) {
                model.addAttribute("msg", "密码错误！");
                return "front/login";
            }
        }
        return "redirect:toRegist";
    }
    @GetMapping("/queryUserByName")
    @ResponseBody
    public String queryUserByName(String name){
        User user = userService.queryUserByUsername(name);
        if (user==null){
            return "canCreate";
        }else{
            return "cannotCreate";
        }
    }
    @GetMapping("/queryUserByEmail")
    @ResponseBody
    public String queryUserByEmail(String email){
        User user = userService.queryUserByEmail(email);
        if (user==null){
            return "canCreate";
        }else{
            return "cannotCreate";
        }
    }
}
