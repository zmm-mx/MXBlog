package com.mxblog.controller;

import com.mxblog.service.BlogColumnServiceImpl;
import com.mxblog.service.BlogServiceImpl;
import com.mxblog.service.BlogTagServiceImpl;
import com.mxblog.service.ViewServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class RouteController {
    @Autowired
    private BlogServiceImpl blogService;
    @Autowired
    private BlogColumnServiceImpl blogColumnService;
    @Autowired
    private BlogTagServiceImpl blogTagService;
    @Autowired
    private ViewServiceImpl viewService;

    @GetMapping({"/","/index"})
    public String toIndex(){
        return "index";
    }

    @GetMapping("/toBlog/{id}")
    public String toBlog(@PathVariable("id") String id, Model model){
        int blogId = Integer.parseInt(id);
        model.addAttribute("id", blogId);
        //让该博客的浏览量+1
        //blogService.updateBlogViews(blogId);
        //让该博客的浏览量+1
        viewService.updateBlogViews(blogId);
        //让排行榜中该博客的浏览量+1
        viewService.updateBlogViewsInRank(blogId);
        return "front/blog";
    }

    @GetMapping("/blogColumn/{id}")
    public String toBlogColumn(@PathVariable("id") int id, Model model){
        if (id==-1){
            id = blogColumnService.queryAllBlogColumns().get(0).getId();
        }
        model.addAttribute("id", id);
        return "front/blogColumn";
    }

    @GetMapping("/blogTag/{id}")
    public String toBlogTag(@PathVariable("id") int id, Model model){
        if (id==-1){
            id = blogTagService.queryAllBlogTags().get(0).getId();
        }
        model.addAttribute("id", id);
        return "front/blogTag";
    }

    @GetMapping("/archives")
    public String toArchives(){
        return "front/archives";
    }

    @GetMapping("/link")
    public String toLink(){
        return "front/link";
    }

    @GetMapping("/myGame")
    public String toMyGame(){
        return "front/myGame";
    }

    // 这个路径现已废弃，故直接重定向到玄学小站首页
    @GetMapping("/plumBlossom")
    public String toPlumBlossom(){
        return "redirect:/metaphysics";
    }

    @GetMapping("/aboutMe")
    public String toAboutMe(){
        return "front/aboutMe";
    }
}
