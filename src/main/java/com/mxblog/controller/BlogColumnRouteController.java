package com.mxblog.controller;

import com.alibaba.fastjson.JSON;
import com.mxblog.pojo.BlogColumn;
import com.mxblog.service.BlogColumnServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/background")
public class BlogColumnRouteController {
    @Autowired
    private BlogColumnServiceImpl blogColumnService;
    @Value("${pageInfo.pageSize}")
    private int pageSize;
    @Value("${file.uploadFolder}")
    private String uploadFolder;

    @GetMapping("/toColumnManage")
    public String toColumnManage(){
        return "background/columnManage";
    }
    @GetMapping("/getBlogColumns")
    @ResponseBody
    public String getBlogColumns(int startIndex){
        HashMap<String, Integer> map = new HashMap<>();
        map.put("startIndex", (startIndex-1)*pageSize);
        map.put("pageSize", pageSize);
        List<BlogColumn> blogColumns = blogColumnService.queryBlogColumnByPage(map);
        String columnString = JSON.toJSONString(blogColumns);
        return columnString;
    }
    @GetMapping("/getAllBlogColumns")
    @ResponseBody
    public String getAllBlogColumns(){
        List<BlogColumn> blogColumns = blogColumnService.queryAllBlogColumns();
        String columnString = JSON.toJSONString(blogColumns);
        return columnString;
    }
    @GetMapping("/getColumnPageSum")
    @ResponseBody
    public String getColumnPageSum(){
        int number = blogColumnService.countBlogColumn();
        int sum = (int) Math.ceil((double) number/(double) pageSize);
        return ""+sum;
    }
    @GetMapping("/toColumnAdd")
    public String toColumnAdd(){
        return "background/columnAdd";
    }
    @GetMapping("/queryColumnByName")
    @ResponseBody
    public String queryColumnByName(String name){
        BlogColumn blogColumn = blogColumnService.queryBlogColumnByName(name);
        if (blogColumn==null){
            return "canCreate";
        }
        return "cannotCreate";
    }
    @PostMapping("/columnAdd")
    public String columnAdd(BlogColumn blogColumn, @RequestParam("headPicture") MultipartFile file, HttpSession session, Model model) throws IOException {
        //上传路径保存设置
        File realPath = new File(uploadFolder+"columnImage/");
        if (!realPath.exists()){
            realPath.mkdir();
        }
        //获取文件原名
        String fileName = file.getOriginalFilename();
        //获取文件后缀
        String fileType = fileName.substring(fileName.lastIndexOf(".") + 1);
        //设置文件后缀
        blogColumn.setImageType(fileType);
        if(blogColumnService.insertBlogColumn(blogColumn)>0) {
            //通过CommonsMultipartFile的方法直接写文件
            file.transferTo(new File(uploadFolder + "columnImage/", blogColumn.getId() + "." + blogColumn.getImageType()));
            return "redirect:/background/toColumnManage";
        }
        return "redirect:/background/toColumnManage";
    }
    @GetMapping("/toColumnModify/{id}")
    public String toColumnModify(@PathVariable("id") int id, Model model){
        BlogColumn blogColumn = blogColumnService.queryBlogColumnById(id);
        model.addAttribute("blogColumn", blogColumn);
        return "background/columnModify";
    }
    @PostMapping("/columnModify")
    public String columnModify(BlogColumn blogColumn, @RequestParam("headPicture") MultipartFile file, HttpSession session, Model model) throws IOException {
        //上传路径保存设置
        File realPath = new File(uploadFolder+"columnImage/");
        if (!realPath.exists()){
            realPath.mkdir();
        }
        //获取文件原名
        String fileName = file.getOriginalFilename();
        //获取文件后缀
        String fileType = fileName.substring(fileName.lastIndexOf(".") + 1);
        //设置文件后缀
        blogColumn.setImageType(fileType);
        if(blogColumnService.updateBlogColumn(blogColumn)>0) {
            //通过CommonsMultipartFile的方法直接写文件
            file.transferTo(new File(uploadFolder + "columnImage/", blogColumn.getId() + "." + blogColumn.getImageType()));
            return "redirect:/background/toColumnManage";
        }
        return "redirect:/background/columnModify";
    }
    @GetMapping("/columnDelete/{id}")
    @ResponseBody
    public String columnDelete(@PathVariable("id") int id){
        int page = 1;
        int count = blogColumnService.countBlogColumnById(id);
        if(blogColumnService.deleteBlogColumnById(id)>0){
            page = (int) Math.ceil((double) (count - 1) / (double) pageSize);
        }else {
            page = (int) Math.ceil((double) count / (double) pageSize);
        }
        return "" + Math.max(page, 1);
    }
}
