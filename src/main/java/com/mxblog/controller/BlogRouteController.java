package com.mxblog.controller;

import com.alibaba.fastjson.JSON;
import com.mxblog.pojo.*;
import com.mxblog.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/background")
public class BlogRouteController {
    @Autowired
    private BlogServiceImpl blogService;
    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private BlogColumnServiceImpl blogColumnService;
    @Autowired
    private BlogTagServiceImpl blogTagService;
    @Autowired
    private TypeServiceImpl typeService;
    @Autowired
    private ShieldServiceImpl shieldService;
    @Value("${pageInfo.pageSize}")
    private int pageSize;

    @GetMapping("/toBlogManage")
    public String toBlogManage(){
        return "background/blogManage";
    }
    @PostMapping("/getBlogs")
    @ResponseBody
    public String getBlogs(String title, String column, String startIndex){
        HashMap<String, Object> map = new HashMap<>();
        int startIndexNum = Integer.parseInt(startIndex);
        map.put("startIndex", (startIndexNum-1)*pageSize);
        if (title!=null && !title.equals("undefined") && !title.equals("")){
            map.put("title", title);
            System.out.println(title);
        }
        if (column!=null && !column.equals("undefined") && !column.equals("")){
            int columnId = Integer.parseInt(column);
            map.put("columnId", columnId);
            System.out.println(columnId);
        }
        map.put("pageSize", pageSize);
        System.out.println((startIndexNum-1)*pageSize);
        List<Blog> blogs = blogService.queryBlogByPage(map);
        String blogString = JSON.toJSONString(blogs);
        return blogString;
    }
    @PostMapping("/getBlogPageSum")
    @ResponseBody
    public String getBlogPageSum(String title, String column){
        HashMap<String, Object> map = new HashMap<>();
        if (title!=null && !title.equals("undefined") && !title.equals("")){
            map.put("title", title);
        }
        if (column!=null && !column.equals("undefined") && !column.equals("")){
            int columnId = Integer.parseInt(column);
            map.put("columnId", columnId);
        }
        int number = blogService.countBlog(map);
        int sum = (int) Math.ceil((double) number/(double) pageSize);
        return ""+sum;
    }
    @GetMapping("/toBlogAdd")
    public String toBlogAdd(){
        return "background/blogAdd";
    }
    @PostMapping("/blogAdd")
    public String blogAdd(Blog blog, String columnId, String tagIds, String shieldId, String typeId, HttpSession session){
        //添加当前用户
        blog.setUser(userService.queryUserById((Integer) session.getAttribute("userIdSession")));
        //添加专栏
        blog.setBlogColumn(blogColumnService.queryBlogColumnById(Integer.parseInt(columnId)));
        //添加标签（如果有就添加，没有就不添加）
        if (!tagIds.equals("") && tagIds!=null){
            List<BlogTag> tagList = new ArrayList<>();
            for (String tagId : tagIds.split(",")) {
                tagList.add(blogTagService.queryBlogTagById(Integer.parseInt(tagId)));
            }
            blog.setBlogTagList(tagList);
        }
        //添加类别
        blog.setType(typeService.getTypeById(Integer.parseInt(typeId)));
        //添加屏蔽情况
        blog.setShield(shieldService.queryShieldById(Integer.parseInt(shieldId)));
        //添加浏览量
        blog.setViews(0);
        //添加发布时间
        blog.setCreateTime(new Date());
        //插入博客
        blogService.insertBlog(blog);
        //插入博客与标签之间的关联（没有就不插入）
        if (!tagIds.equals("") && tagIds!=null) {
            blogService.insertBlogAndTag(blog);
        }
        return "redirect:/background/toBlogManage";
    }
    @GetMapping("/blogDelete/{id}")
    @ResponseBody
    public String blogDelete(@PathVariable("id") int id){
        int page = 1;
        int count = blogService.countBlogById(id);
        blogService.deleteBlogById(id);
        page = (int) Math.ceil((double) (count - 1) / (double) pageSize);
        return "" + Math.max(page, 1);
    }
    @GetMapping("/blogShield/{id}/{shield}")
    @ResponseBody
    public String blogShield(@PathVariable("id") int id, @PathVariable("shield") int shield){
        int page = 1;
        int count = blogService.countBlogById(id);
        HashMap<String, Integer> map = new HashMap<>();
        map.put("id", id);
        map.put("shieldId", shield);
        blogService.shieldBlog(map);
        page = (int) Math.ceil((double) (count - 1) / (double) pageSize);
        return "" + Math.max(page, 1);
    }
    @GetMapping("/toBlogModify/{id}")
    public String toBlogModify(@PathVariable("id") int id, Model model){
        Blog blog = blogService.queryBlogById(id);
        model.addAttribute("blog", blog);
        String idList = blogTagService.getIdList(blog.getBlogTagList());
        model.addAttribute("tagList", idList);
        return "background/blogModify";
    }
    @PostMapping("/blogModify")
    public String blogModify(Blog blog, String columnId, String tagIds, String shieldId, String typeId, HttpSession session) {
        //添加专栏
        blog.setBlogColumn(blogColumnService.queryBlogColumnById(Integer.parseInt(columnId)));
        //添加标签（如果有就添加，没有就不添加）
        if (!tagIds.equals("") && tagIds!=null){
            List<BlogTag> tagList = new ArrayList<>();
            for (String tagId : tagIds.split(",")) {
                tagList.add(blogTagService.queryBlogTagById(Integer.parseInt(tagId)));
            }
            blog.setBlogTagList(tagList);
        }
        //添加类别
        blog.setType(typeService.getTypeById(Integer.parseInt(typeId)));
        //添加屏蔽情况
        blog.setShield(shieldService.queryShieldById(Integer.parseInt(shieldId)));
        //修改博客
        blogService.updateBlog(blog);
        //删除博客与标签之间的关联
        blogService.deleteBlogAndTagById(blog.getId());
        //插入博客与标签之间的关联（没有就不插入）
        if (!tagIds.equals("") && tagIds!=null) {
            blogService.insertBlogAndTag(blog);
        }
        return "redirect:/background/toBlogManage";
    }
}
