package com.mxblog.controller;

import com.alibaba.fastjson.JSON;
import com.mxblog.pojo.Game;
import com.mxblog.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class GameRouteController {
	@Autowired
	private GameService gameService;

	@GetMapping("/getCountsOfGames")
	@ResponseBody
	public String getCountsOfGames() {
		int nums = gameService.queryCountsOfGames();
		return JSON.toJSONString(nums);
	}

	@GetMapping("/getAllGames")
	@ResponseBody
	public String getAllGames() {
		List<Game> games = gameService.queryAllGames();
		return JSON.toJSONString(games);
	}
}
