package com.mxblog.controller;

import com.alibaba.fastjson.JSON;
import com.mxblog.pojo.Type;
import com.mxblog.service.TypeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/background")
public class TypeRouteController {
    @Autowired
    private TypeServiceImpl typeService;

    @GetMapping("/getAllTypes")
    @ResponseBody
    public String getAllTypes(){
        List<Type> types = typeService.getAllTypes();
        String typeString = JSON.toJSONString(types);
        return typeString;
    }
}
