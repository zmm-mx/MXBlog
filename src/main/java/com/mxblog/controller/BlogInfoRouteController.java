package com.mxblog.controller;

import com.alibaba.fastjson.JSON;
import com.mxblog.pojo.Blog;
import com.mxblog.service.*;
import com.mxblog.utils.MarkdownUtils;
import com.mxblog.utils.RedisUtil;
import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

//博客展示页面
@Controller
public class BlogInfoRouteController {
    @Autowired
    private BlogServiceImpl blogService;
    @Autowired
    private RedisUtil redisUtil;

    @GetMapping("/queryBlogByIdWithCommonMark/{id}")
    @ResponseBody
    public String queryBlogByIdWithCommonMark(@PathVariable("id") String id) throws NotFoundException {
        Blog blog = blogService.queryBlogById(Integer.parseInt(id));
        if (blog==null){
            throw new NotFoundException("博客不存在");
        }
        String content = blog.getContent();
        blog.setContent(MarkdownUtils.markdownToHtmlExtensions(content));
        // 读取Redis设置浏览量
        Object views = redisUtil.get(blog.getRedisKeyWithId());
        if (views != null) {
            blog.setViews((Integer) views);
        }
        else {
            blog.setViews(0);
        }
        String blogString = JSON.toJSONString(blog);
        return blogString;
    }
}
