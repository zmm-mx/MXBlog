package com.mxblog.controller;

import com.alibaba.fastjson.JSON;
import com.mxblog.pojo.Blog;
import com.mxblog.pojo.Comment;
import com.mxblog.service.BlogServiceImpl;
import com.mxblog.service.CommentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/background")
public class CommentBackgroundRouteController {
    @Autowired
    private CommentServiceImpl commentService;
    @Autowired
    private BlogServiceImpl blogService;

    @Value("${pageInfo.pageSize}")
    private int pageSize;

    @GetMapping("/toCommentManage")
    public String toCommentManage(){
        return "background/commentManage";
    }

    @PostMapping("/getComments")
    @ResponseBody
    public String getComments(@RequestParam("blogId") String id, String startIndex){
        HashMap<String, Object> map = new HashMap<>();
        int startIndexNum = Integer.parseInt(startIndex);
        map.put("startIndex", (startIndexNum-1)*pageSize);
        if (id!=null && !id.equals("undefined") && !id.equals("")){
            int blogId = Integer.parseInt(id);
            map.put("blogId", blogId);
        }
        map.put("pageSize", pageSize);
        List<Comment> comments = commentService.queryCommentsByPageWithBlogId(map);
        String commentString = JSON.toJSONString(comments);
        return commentString;
    }

    @PostMapping("/getCommentPageSum")
    @ResponseBody
    public String getCommentPageSum(@RequestParam("blogId") String id){
        HashMap<String, Object> map = new HashMap<>();
        if (id!=null && !id.equals("undefined") && !id.equals("")){
            int blogId = Integer.parseInt(id);
            map.put("blogId", blogId);
        }
        int number = commentService.countComment(map);
        int sum = (int) Math.ceil((double) number/(double) pageSize);
        return ""+sum;
    }

    @GetMapping("/getAllBlogs")
    @ResponseBody
    public String getAllBlogs(){
        List<Blog> blogs = blogService.queryAllBlogs();
        String blogString = JSON.toJSONString(blogs);
        return blogString;
    }

    @GetMapping("/commentShield/{id}/{shield}")
    @ResponseBody
    public String commentShield(@PathVariable("id") int id, @PathVariable("shield") int shield){
        HashMap<String, Integer> map = new HashMap<>();
        map.put("id", id);
        map.put("shieldId", shield);
        commentService.shieldComment(map);
        return "";
    }
}
