package com.mxblog.controller;

import com.alibaba.fastjson.JSON;
import com.mxblog.pojo.Blog;
import com.mxblog.pojo.BlogColumn;
import com.mxblog.service.BlogColumnServiceImpl;
import com.mxblog.service.BlogServiceImpl;
import com.mxblog.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

@Controller
public class ColumnRouteController {
    @Autowired
    private BlogColumnServiceImpl blogColumnService;
    @Autowired
    private BlogServiceImpl blogService;
    @Autowired
    private RedisUtil redisUtil;

    @Value("${pageInfo.pageSize}")
    private int pageSize;

    @GetMapping("/getColumnCount")
    @ResponseBody
    public String getColumnCount(){
        int number = blogColumnService.countBlogColumn();
        return ""+number;
    }

    @PostMapping("/getBlogsByPageWithColumnIdWithoutShield")
    @ResponseBody
    public String getBlogsByPageWithColumnIdWithoutShield(int startIndex, int columnId){
        HashMap<String, Object> map = new HashMap<>();
        map.put("startIndex", (startIndex-1)*pageSize);
        map.put("pageSize", pageSize);
        map.put("columnId", columnId);
        List<Blog> blogs = blogService.getBlogsByPageWithColumnIdWithoutShield(map);
        // 读取Redis设置浏览量
        for (Blog blog : blogs) {
            Object views = redisUtil.get(blog.getRedisKeyWithId());
            if (views != null) {
                blog.setViews((Integer) views);
            }
            else {
                blog.setViews(0);
            }
        }
        String blogsString = JSON.toJSONString(blogs);
        return blogsString;
    }

    @PostMapping("/getBlogPageSumWithColumnIdWithoutShield")
    @ResponseBody
    public String getBlogPageSumWithColumnIdWithoutShield(String columnId){
        int number = blogService.countBlogWithColumnIdWithoutShield(Integer.parseInt(columnId));
        int sum = (int) Math.ceil((double) number/(double) pageSize);
        return ""+sum;
    }

    @GetMapping("/getBlogColumnsWithoutShield")
    @ResponseBody
    public String getBlogColumnsWithoutShield(){
        List<BlogColumn> blogColumns = blogColumnService.queryAllBlogColumns();
        for (BlogColumn blogColumn : blogColumns) {
            blogColumn.setBlogSum(blogService.queryBlogSumByColumnIdWithoutShield(blogColumn.getId()));
        }
        String columnString = JSON.toJSONString(blogColumns);
        return columnString;
    }
}
