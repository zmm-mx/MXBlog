package com.mxblog.controller;

import com.alibaba.fastjson.JSON;
import com.mxblog.pojo.Blog;
import com.mxblog.pojo.BlogTag;
import com.mxblog.service.BlogServiceImpl;
import com.mxblog.service.BlogTagServiceImpl;
import com.mxblog.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

@Controller
public class TagRouteController {
    @Autowired
    private BlogTagServiceImpl blogTagService;
    @Autowired
    private BlogServiceImpl blogService;
    @Autowired
    private RedisUtil redisUtil;

    @Value("${pageInfo.pageSize}")
    private int pageSize;

    @GetMapping("/getBlogTagsWithoutShield")
    @ResponseBody
    public String getBlogTagsWithoutShield(){
        List<BlogTag> blogTags = blogTagService.queryAllBlogTags();
        for (BlogTag blogTag : blogTags) {
            blogTag.setBlogSum(blogService.queryBlogSumByTagIdWithoutShield(blogTag.getId()));
        }
        String tagString = JSON.toJSONString(blogTags);
        return tagString;
    }

    @GetMapping("/getTagCount")
    @ResponseBody
    public String getTagCount(){
        int number = blogTagService.countBlogTag();
        return ""+number;
    }

    @PostMapping("/getBlogsByPageWithTagIdWithoutShield")
    @ResponseBody
    public String getBlogsByPageWithTagIdWithoutShield(int startIndex, int tagId){
        HashMap<String, Object> map = new HashMap<>();
        map.put("startIndex", (startIndex-1)*pageSize);
        map.put("pageSize", pageSize);
        map.put("tagId", tagId);
        List<Blog> blogs = blogService.getBlogsByPageWithTagIdWithoutShield(map);
        // 读取Redis设置浏览量
        for (Blog blog : blogs) {
            Object views = redisUtil.get(blog.getRedisKeyWithId());
            if (views != null) {
                blog.setViews((Integer) views);
            }
            else {
                blog.setViews(0);
            }
        }
        String blogsString = JSON.toJSONString(blogs);
        return blogsString;
    }

    @PostMapping("/getBlogPageSumWithTagIdWithoutShield")
    @ResponseBody
    public String getBlogPageSumWithTagIdWithoutShield(String tagId){
        int number = blogService.queryBlogSumByTagIdWithoutShield(Integer.parseInt(tagId));
        int sum = (int) Math.ceil((double) number/(double) pageSize);
        return ""+sum;
    }
}
