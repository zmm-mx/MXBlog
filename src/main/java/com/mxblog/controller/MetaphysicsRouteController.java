package com.mxblog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MetaphysicsRouteController {
	@GetMapping("/metaphysics")
	public String toMetaphysics(){
		return "front/metaphysics";
	}

	@GetMapping("/metaphysics/plumBlossom")
	public String toPlumBlossom(){
		return "front/plumBlossom";
	}

	@GetMapping("/metaphysics/fourPillars")
	public String toFourPillars(){
		return "front/fourPillars";
	}

	@GetMapping("/metaphysics/sixLines")
	public String toSixLines(){
		return "front/sixLines";
	}

	@GetMapping("/metaphysics/aboutMe")
	public String toAboutXiao(){
		return "front/aboutXiao";
	}
}
