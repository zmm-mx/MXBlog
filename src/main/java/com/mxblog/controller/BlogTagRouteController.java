package com.mxblog.controller;

import com.alibaba.fastjson.JSON;
import com.mxblog.pojo.BlogColumn;
import com.mxblog.pojo.BlogTag;
import com.mxblog.service.BlogTagServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/background")
public class BlogTagRouteController {
    @Autowired
    private BlogTagServiceImpl blogTagService;
    @Value("${pageInfo.pageSize}")
    private int pageSize;

    @GetMapping("/toTagManage")
    public String toTagManage(){
        return "background/tagManage";
    }
    @GetMapping("/getBlogTags")
    @ResponseBody
    public String getBlogTags(int startIndex, Model model){
        HashMap<String, Integer> map = new HashMap<>();
        map.put("startIndex", (startIndex-1)*pageSize);
        map.put("pageSize", pageSize);
        List<BlogTag> blogTags = blogTagService.queryBlogTagByPage(map);
        String tagString = JSON.toJSONString(blogTags);
        return tagString;
    }
    @GetMapping("/getAllBlogTags")
    @ResponseBody
    public String getAllBlogTags(){
        List<BlogTag> blogTags = blogTagService.queryAllBlogTags();
        String tagString = JSON.toJSONString(blogTags);
        return tagString;
    }
    @GetMapping("/getTagPageSum")
    @ResponseBody
    public String getTagPageSum(){
        int number = blogTagService.countBlogTag();
        int sum = (int) Math.ceil((double) number/(double) pageSize);
        return ""+sum;
    }
    @GetMapping("/toTagAdd")
    public String toTagAdd(){
        return "background/tagAdd";
    }
    @GetMapping("/queryTagByName")
    @ResponseBody
    public String queryTagByName(String name){
        BlogTag blogTag = blogTagService.queryBlogTagByName(name);
        if (blogTag==null){
            return "canCreate";
        }
        return "cannotCreate";
    }
    @GetMapping("/queryTagByBlogId/{id}")
    @ResponseBody
    public String queryTagByBlogId(@PathVariable("id") String id){
        List<BlogTag> blogTags = blogTagService.queryBlogTagByBlogId(Integer.parseInt(id));
        String tagString = JSON.toJSONString(blogTags);
        return tagString;
    }
    @PostMapping("/tagAdd")
    public String tagAdd(BlogTag blogTag) {
        if(blogTagService.insertBlogTag(blogTag)>0) {
            return "redirect:/background/toTagManage";
        }
        return "redirect:/background/toTagManage";
    }
    @GetMapping("/toTagModify/{id}")
    public String toTagModify(@PathVariable("id") int id, Model model){
        BlogTag blogTag = blogTagService.queryBlogTagById(id);
        model.addAttribute("blogTag", blogTag);
        return "background/tagModify";
    }
    @PostMapping("/tagModify")
    public String tagModify(BlogTag blogTag) {
        if(blogTagService.updateBlogTag(blogTag)>0) {
            return "redirect:/background/toTagManage";
        }
        return "redirect:/background/toTagManage";
    }
    @GetMapping("/tagDelete/{id}")
    @ResponseBody
    public String tagDelete(@PathVariable("id") int id){
        int page = 1;
        int count = blogTagService.countBlogTagById(id);
        blogTagService.deleteBlogTagById(id);
        page = (int) Math.ceil((double) (count - 1) / (double) pageSize);
        return "" + Math.max(page, 1);
    }
}
