package com.mxblog.controller;

import com.alibaba.fastjson.JSON;
import com.mxblog.pojo.BlogColumn;
import com.mxblog.pojo.BlogTag;
import com.mxblog.pojo.Link;
import com.mxblog.service.LinkServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/background")
public class LinkBackgroundRouteController {
    @Autowired
    private LinkServiceImpl linkService;

    @Value("${pageInfo.pageSize}")
    private int pageSize;

    @GetMapping("/toLinkManage")
    public String toLinkManage(){
        return "background/linkManage";
    }

    @GetMapping("/getLinks")
    @ResponseBody
    public String getLinks(int startIndex){
        HashMap<String, Integer> map = new HashMap<>();
        map.put("startIndex", (startIndex-1)*pageSize);
        map.put("pageSize", pageSize);
        List<Link> links = linkService.queryLinkByPage(map);
        String linkString = JSON.toJSONString(links);
        return linkString;
    }

    @GetMapping("/getLinkPageSum")
    @ResponseBody
    public String getLinkPageSum(){
        int number = linkService.countLink();
        int sum = (int) Math.ceil((double) number/(double) pageSize);
        return ""+sum;
    }

    @GetMapping("/linkDelete/{id}")
    @ResponseBody
    public String linkDelete(@PathVariable("id") int id){
        int page = 1;
        int count = linkService.countLinkById(id);
        if(linkService.deleteLinkById(id)>0){
            page = (int) Math.ceil((double) (count - 1) / (double) pageSize);
        }else {
            page = (int) Math.ceil((double) count / (double) pageSize);
        }
        return "" + Math.max(page, 1);
    }

    @GetMapping("/toLinkAdd")
    public String toColumnAdd(){
        return "background/linkAdd";
    }

    @GetMapping("/queryLinkByBlogLink")
    @ResponseBody
    public String queryLinkByBlogLink(String blogLink){
        Link link = linkService.queryLinkByBlogLink(blogLink);
        if (link==null){
            return "canCreate";
        }
        return "cannotCreate";
    }
    @PostMapping("/linkAdd")
    public String linkAdd(Link link) {
        if(linkService.insertLink(link)>0) {
            return "redirect:/background/toLinkManage";
        }
        return "redirect:/background/toLinkManage";
    }
    @GetMapping("/toLinkModify/{id}")
    public String toLinkModify(@PathVariable("id") int id, Model model){
        Link link = linkService.queryLinkById(id);
        model.addAttribute("link", link);
        return "background/linkModify";
    }
    @PostMapping("/linkModify")
    public String linkModify(Link link) {
        if(linkService.updateLink(link)>0) {
            return "redirect:/background/toLinkManage";
        }
        return "redirect:/background/toLinkManage";
    }
}
