package com.mxblog.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.mxblog.pojo.Blog;
import com.mxblog.pojo.Comment;
import com.mxblog.pojo.User;
import com.mxblog.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class CommentRouteController {
    @Autowired
    private CommentServiceImpl commentService;
    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private BlogServiceImpl blogService;
    @Autowired
    private ShieldServiceImpl shieldService;
    @Autowired
    private JavaMailSenderImpl mailSender;

    @GetMapping("/queryAllCommentsByBlogIdNoParent/{blogId}")
    @ResponseBody
    public String queryAllCommentsByBlogIdNoParent(@PathVariable int blogId){
        List<Comment> comments = commentService.queryAllCommentsByBlogIdNoParent(blogId);
        List<Comment> parentComments = eachComment(comments);
        System.out.println(parentComments);
        String commentsString = JSON.toJSONString(parentComments, SerializerFeature.DisableCircularReferenceDetect);
        return commentsString;
    }

    @PostMapping("/insertComment")
    @ResponseBody
    public String insertComment(String content, String parentCommentId, String blogId, HttpSession session) throws MessagingException {
        Comment comment = new Comment();
        comment.setContent(content);
        comment.setParentCommentId(Integer.parseInt(parentCommentId));
        User commentUser = userService.queryUserById((Integer) session.getAttribute("userIdSession"));
        comment.setUser(commentUser);
        Blog commentBlog = blogService.queryBlogById(Integer.parseInt(blogId));
        comment.setBlog(commentBlog);
        comment.setCreateTime(new Date());
        comment.setShield(shieldService.queryShieldById(1));
        commentService.insertComment(comment);
        return queryAllCommentsByBlogIdNoParent(Integer.parseInt(blogId));
    }

    @PostMapping("/sendEmail")
    @ResponseBody
    public String sendEmail(String content, String parentCommentId, String blogId, HttpSession session) throws MessagingException {
        User commentUser = userService.queryUserById((Integer) session.getAttribute("userIdSession"));
        Blog commentBlog = blogService.queryBlogById(Integer.parseInt(blogId));
        // 发邮件通知
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setSubject("【妙霄的博客】你收到一条回复");
        helper.setText("<p>来自用户 <b>" + commentUser.getUsername() + "</b> 在博客 <b>《" + commentBlog.getTitle() + "》</b> 下对您的回复：</p><p><b>" + content + "</b></p><p>点击链接可进行跳转：<a href='http://www.mxblog.top/toBlog/" + commentBlog.getId() + "' target='_blank'>" + commentBlog.getTitle() + "</a></p>", true);
        if (parentCommentId.equals("-1")) {
            helper.setTo("19852821117@163.com");
        }
        else {
            Comment parentComment = commentService.queryCommentById(Integer.parseInt(parentCommentId));
            helper.setTo(parentComment.getUser().getEmail());
        }
        helper.setFrom("19852821117@163.com");
        mailSender.send(message);
        return queryAllCommentsByBlogIdNoParent(Integer.parseInt(blogId));
    }

    @GetMapping("/countCommentsByBlogId/{blogId}")
    @ResponseBody
    public String countCommentsByBlogId(@PathVariable String blogId){
        int id = Integer.parseInt(blogId);
        int count = commentService.countCommentsByBlogId(id);
        return ""+count;
    }

    //存放迭代找出的所有子代的集合
    private List<Comment> tempReplys = new ArrayList<>();

    /**
     * 循环每个顶级评论节点
     * @param comments
     * @return
     */
    private List<Comment> eachComment(List<Comment> comments) {
        List<Comment> commentsView = new ArrayList<>();
        for (Comment comment : comments) {
            Comment c = new Comment();
            BeanUtils.copyProperties(comment, c);
            commentsView.add(c);
        }
        //合并评论的各层子代到第一级子代集合中
        combineChildren(commentsView);
        return commentsView;
    }

    /**
     * root根节点，blog不为空的对象集合
     * @param comments
     */
    private void combineChildren(List<Comment> comments) {
        for (Comment comment : comments) {
            List<Comment> replys = comment.getReplyComments();
            for(Comment reply : replys){
                //循环迭代，找出子代，存放在tempReplys中
                recursively(reply);
            }
            //清除子评论的所有子评论
            for (Comment tempReply : tempReplys) {
                tempReply.setReplyComments(null);
            }
            //修改顶级节点的reply集合为迭代处理后的集合
            comment.setReplyComments(tempReplys);
            //清除临时存放区
            tempReplys = new ArrayList<>();
        }
    }

    /**
     * 递归迭代
     * @param comment
     */
    private void recursively(Comment comment) {
        tempReplys.add(comment);    //顶节点添加到临时存放集合
        if (comment.getReplyComments().size()>0){
            List<Comment> replys = comment.getReplyComments();
            for (Comment reply : replys) {
                recursively(reply);
            }
        }
    }
}
