package com.mxblog.mapper;

import com.mxblog.pojo.Blog;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface SearchMapper extends ElasticsearchRepository<Blog, Long> {
}
