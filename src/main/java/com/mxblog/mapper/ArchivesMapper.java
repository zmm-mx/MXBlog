package com.mxblog.mapper;

import com.mxblog.pojo.Archives;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ArchivesMapper {
    //获取所有博客均未被屏蔽的年份
    public List<String> queryAllYearsWithoutShield();
    //按照年份获取所有未被屏蔽的博客
    public List<Archives> queryBlogsByYearWithoutShield(String year);
}
