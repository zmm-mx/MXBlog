package com.mxblog.mapper;

import com.mxblog.pojo.BlogTag;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface BlogTagMapper {
    //分页查询博客标签
    public List<BlogTag> queryBlogTagByPage(Map<String, Integer> pageInfo);
    //查询所有博客标签
    public List<BlogTag> queryAllBlogTags();
    //统计标签总量
    public int countBlogTag();
    //按照id统计标签数量
    public int countBlogTagById(int id);
    //按照名称查询标签
    public BlogTag queryBlogTagByName(String name);
    //按照博客id查询其所有标签
    public List<BlogTag> queryBlogTagByBlogId(int id);
    //按照id查询单个标签
    public BlogTag queryBlogTagById(int id);
    //添加一个标签
    public int insertBlogTag(BlogTag blogTag);
    //根据id删除一个标签
    public int deleteBlogTagById(int id);
    //修改一个标签
    public int updateBlogTag(BlogTag blogTag);
}
