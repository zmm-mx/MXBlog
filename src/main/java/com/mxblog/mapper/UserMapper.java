package com.mxblog.mapper;

import com.mxblog.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface UserMapper {
    //通过用户名查询用户
    public User queryUserByUsername(String username);
    //通过ID查询用户
    public User queryUserById(int id);
    //新增用户
    public int insertUser(User user);
    //查询最末尾的id
    public int getMaxId();
    //通过邮箱查找用户
    public User queryUserByEmail(String email);
    //分页查找用户
    public List<User> queryUserByPage(Map<String, Object> map);
    //统计用户总量
    public int countUsers();
    //根据id清除用户名
    public int clearUsernameById(Map<String, Object> map);
    //根据id修改用户的头像类型为jpg
    public int changeUserAvatarTypeToJPG(int id);
}
