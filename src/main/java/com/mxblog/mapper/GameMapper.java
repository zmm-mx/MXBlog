package com.mxblog.mapper;

import com.mxblog.pojo.Game;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface GameMapper {
	// 获取游戏作品的数量
	int queryCountsOfGames();
	// 获取所有的游戏作品的信息
	List<Game> queryAllGames();
}
