package com.mxblog.mapper;

import com.mxblog.pojo.Blog;
import com.mxblog.pojo.BlogColumn;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface BlogColumnMapper {
    //分页查询博客专栏
    public List<BlogColumn> queryBlogColumnByPage(Map<String, Integer> pageInfo);
    //获取全部博客专栏
    public List<BlogColumn> queryAllBlogColumns();
    //统计专栏总量
    public int countBlogColumn();
    //根据id统计专栏数量
    public int countBlogColumnById(int id);
    //按照名称查询专栏
    public BlogColumn queryBlogColumnByName(String name);
    //按照id查询单个专栏
    public BlogColumn queryBlogColumnById(int id);
    //添加一个专栏
    public int insertBlogColumn(BlogColumn blogColumn);
    //根据id删除一个专栏
    public int deleteBlogColumnById(int id);
    //修改一个专栏
    public int updateBlogColumn(BlogColumn blogColumn);
}
