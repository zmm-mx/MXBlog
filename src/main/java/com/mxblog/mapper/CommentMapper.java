package com.mxblog.mapper;

import com.mxblog.pojo.Comment;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Mapper
@Repository
public interface CommentMapper {
    //根据博客id搜索全部父级评论
    public List<Comment> queryAllCommentsByBlogIdNoParent(int blogId);
    //插入一个评论
    public int insertComment(Comment comment);
    //根据id查找评论
    public Comment queryCommentById(int id);
    //根据博客id查找评论数量
    public int countCommentsByBlogId(int blogId);
    //根据博客id分页查询评论
    public List<Comment> queryCommentsByPageWithBlogId(Map<String, Object> map);
    //查询评论数量
    public int countComment(Map<String, Object> map);
    //根据id查询评论是第几条
    public int countCommentById(int id);
    //根据id屏蔽/解除一个评论
    public int shieldComment(Map<String, Integer> shieldInfo);
}
