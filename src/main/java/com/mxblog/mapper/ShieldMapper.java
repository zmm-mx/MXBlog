package com.mxblog.mapper;

import com.mxblog.pojo.Shield;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface ShieldMapper {
    //按照id查询是否屏蔽
    public Shield queryShieldById(int id);
}
