package com.mxblog.mapper;

import com.mxblog.pojo.Diagram;
import com.mxblog.pojo.Tone;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface MetaphysicsMapper {
	Diagram getDiagramById(@Param("id") int id);
	Tone getToneByStemIdAndBranchId(@Param("stemId") int stemId, @Param("branchId") int branchId);
}
