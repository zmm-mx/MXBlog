package com.mxblog.mapper;

import com.mxblog.pojo.Type;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TypeMapper {
    //获取所有类别
    public List<Type> getAllTypes();
    //根据Id获取类别
    public Type getTypeById(int id);
}
