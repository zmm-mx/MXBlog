package com.mxblog.vo;

import com.mxblog.pojo.FourPillars;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FourPillarsVO {
	private String date;
	private String time;
	private FourPillars fourPillars;
	private String code;
}
