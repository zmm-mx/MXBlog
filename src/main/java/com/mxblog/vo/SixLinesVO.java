package com.mxblog.vo;

import com.mxblog.pojo.SixLines;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SixLinesVO {
	private String dateTime;
	private String dateTimeLunar;
	private int[] linesArray;
	private SixLines sixLines;
	private String code;
}
