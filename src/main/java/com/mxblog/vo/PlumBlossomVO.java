package com.mxblog.vo;

import com.mxblog.pojo.Diagram;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

// 用于在前端渲染的数据
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlumBlossomVO {
	private String character1;
	private String character2;
	private String date;
	private String dateLunar;
	private String situation;
	private List<Diagram> diagrams;
	private String code;
}
