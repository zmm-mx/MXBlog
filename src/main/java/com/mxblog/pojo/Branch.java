package com.mxblog.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// 地支
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Branch {
	private int id;
	private String name;
	private String animal;
	private Element element;
}
