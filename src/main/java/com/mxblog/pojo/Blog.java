package com.mxblog.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = "blog", shards = 3, replicas = 1)
public class Blog {
    //编号
    @Id
    private int id;
    //标题
    @Field(type = FieldType.Text, analyzer = "ik_max_word")
    private String title;
    //内容
    private String content;
    //摘要
    private String summary;
    //博客专栏
    private BlogColumn blogColumn;
    //博客标签
    private List<BlogTag> blogTagList;
    //博客类型（原创、转载）
    private Type type;
    //浏览量
    @Field(type = FieldType.Keyword, index = false)
    private int views;
    //创建时间
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    //是否屏蔽（如果只是保存，就默认屏蔽；也可以将已发布的重新屏蔽；被屏蔽的也随时可以解除屏蔽）
    private Shield shield;
    //作者（用户）
    private User user;

    // 返回带有 id 的 Redis key
    public String getRedisKeyWithId() {
        return "mxblog:view:blog:" + this.id;
    }
}
