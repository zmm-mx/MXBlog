package com.mxblog.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 爻
 * @Author 妙霄
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Yao {
	private String valueLow;
	private String valueHigh;
	private YinYang yinYang;
}