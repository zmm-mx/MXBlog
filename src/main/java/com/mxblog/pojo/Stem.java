package com.mxblog.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// 天干
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Stem {
	private int id;
	private String name;
	private Element element;
	private YinYang yinYang;
}
