package com.mxblog.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 五行
 * @Author 妙霄
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Element {
	private int id;
	private String name;
	private int generation;
	private int restriction;
	private String color;
}
