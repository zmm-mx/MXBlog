package com.mxblog.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SixLines {
	// 本卦和变卦的卦象
	private Diagram[] diagrams;
}
