package com.mxblog.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Game {
	// 主键 ID
	private int id;
	// 原创性
	private String originality;
	// 游戏名称
	private String name;
	// 游戏类型
	private String type;
	// 建立项目的日期
	@JSONField(format="yyyy年MM月dd日")
	private Date startTime;
	// 最近更新的日期
	@JSONField(format="yyyy年MM月dd日")
	private Date endTime;
	// 游戏介绍
	private String introduction;
	// 作者有话说
	private String chat;
	// 视频链接
	private String videoLink;
}
