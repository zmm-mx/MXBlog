package com.mxblog.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlogTag {
    //编号
    private int id;
    //名称
    private String name;
    //博客编号
    private List<Blog> blogList;
    //该标签下有多少博客
    private int blogSum;
}
