package com.mxblog.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 六条爻的卦象（六十四卦）
 * @Author 妙霄
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Diagram {
	// 数据库里的主键
	private int id;
	// 类型：本卦/互卦/变卦
	private String type;
	// 卦名
	private String name;
	// 处在哪一宫
	private int palace;
	// 宫名
	private String palaceName;
	// 处在宫内第几位
	private int position;
	// 世爻是第几爻（数字从上往下）
	private int shiYaoNum;
	// 应爻是第几爻
	private int yingYaoNum;
	// 世爻决定的名称
	private String shiYaoName;
	// 上下两个卦象
	private Trigram[] trigrams = new Trigram[2];
	// 纳甲
	private StemAndBranch[] storageJia;
	// 六亲
	private String[] sixRelations;
	// 六神
	private String[] sixGods;
	// 卦身
	private Branch bodyBranch;
	// 吉凶
	private String luck;
	// 古文解释
	private String ancient;
	// 现代文解释
	private String modern;
	// 卦象解释
	private String explanation;
}