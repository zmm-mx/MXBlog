package com.mxblog.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Comment {
    //编号
    private int id;
    //发表评论的用户
    private User user;
    //隶属于哪篇博客
    private Blog blog;
    //内容
    private String content;
    //发表时间
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    //父评论id
    private int parentCommentId;
    //父评论
    private Comment parentComment;
    //子评论列表
    private List<Comment> replyComments;
    //屏蔽情况
    private Shield shield;
}
