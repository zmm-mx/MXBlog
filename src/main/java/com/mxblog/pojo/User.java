package com.mxblog.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    //编号
    private int id;
    //用户名
    private String username;
    //密码
    private String password;
    //邮箱
    private String email;
    //权限
    private String authority;
    //头像类型（头像图片的名称与编号相同，例如 "avatar1.png"，因此只用保存类型）
    private String avatarType;
    //创建时间
    @JSONField(format="yyyy-MM-dd")
    private Date createTime;
}
