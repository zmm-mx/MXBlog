package com.mxblog.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FourPillars {
	StemAndBranch[] stemAndBranches = new StemAndBranch[4];
	Tone[] tones = new Tone[4];
	String[] tenGods = new String[4];
}
