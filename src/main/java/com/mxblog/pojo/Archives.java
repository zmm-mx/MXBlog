package com.mxblog.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Archives {
    private int id;
    private String title;
    private Type type;
    private String createYear;
    private String createMonthAndDay;
}
