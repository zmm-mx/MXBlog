package com.mxblog.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// 一组干支
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StemAndBranch {
	private Stem stem;
	private Branch branch;
}
