package com.mxblog.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 三根爻组成的卦（八卦）
 * @Author 妙霄
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Trigram {
	private int id;
	private String name;
	private Yao[] yaos = new Yao[3];
	private Element element;
}