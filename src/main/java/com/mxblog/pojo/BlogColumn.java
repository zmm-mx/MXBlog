package com.mxblog.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlogColumn {
    //编号
    private int id;
    //名称
    private String name;
    //首图类型
    private String imageType;
    //该专栏下总共有多少篇博客
    private int blogSum;
}
