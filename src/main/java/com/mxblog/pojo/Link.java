package com.mxblog.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Link {
    //编号
    private int id;
    //标题
    private String title;
    //简介
    private String description;
    //头像链接
    private String avatarLink;
    //博客链接
    private String blogLink;
}
