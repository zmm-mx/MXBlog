package com.mxblog.service;

import com.mxblog.mapper.CommentMapper;
import com.mxblog.pojo.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentMapper mapper;

    //根据博客id搜索全部父级评论
    @Override
    public List<Comment> queryAllCommentsByBlogIdNoParent(int blogId) {
        return mapper.queryAllCommentsByBlogIdNoParent(blogId);
    }
    //插入一个评论
    @Override
    public int insertComment(Comment comment) {
        return mapper.insertComment(comment);
    }
    //根据id查找评论
    @Override
    //根据id查找评论
    public Comment queryCommentById(int id){
        return mapper.queryCommentById(id);
    }
    @Override
    //根据博客id查找评论数量
    public int countCommentsByBlogId(int blogId){
        return mapper.countCommentsByBlogId(blogId);
    }
    @Override
    //根据博客id分页查询评论
    public List<Comment> queryCommentsByPageWithBlogId(Map<String, Object> map){
        return mapper.queryCommentsByPageWithBlogId(map);
    }
    @Override
    //查询评论数量
    public int countComment(Map<String, Object> map){
        return mapper.countComment(map);
    }
    @Override
    //根据id查询评论是第几条
    public int countCommentById(int id){
        return mapper.countCommentById(id);
    }
    @Override
    //根据id屏蔽/解除一个评论
    public int shieldComment(Map<String, Integer> shieldInfo){
        return mapper.shieldComment(shieldInfo);
    }
}
