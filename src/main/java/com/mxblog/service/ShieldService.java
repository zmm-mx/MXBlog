package com.mxblog.service;

import com.mxblog.pojo.Shield;

public interface ShieldService {
    //按照id查询是否屏蔽
    public Shield queryShieldById(int id);
}
