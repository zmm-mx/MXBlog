package com.mxblog.service;

import com.mxblog.vo.FourPillarsVO;

import java.io.UnsupportedEncodingException;
import java.util.List;

public interface FourPillarsService {
	FourPillarsVO getFourPillarsVO(String dateString);
	String getCode(String dateString);
	String subCode(String code);
	String getDateAndTime(String code);
}
