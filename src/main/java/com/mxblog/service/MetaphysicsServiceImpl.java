package com.mxblog.service;

import com.mxblog.constants.MetaphysicsConstants;
import com.mxblog.mapper.MetaphysicsMapper;
import com.mxblog.pojo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class MetaphysicsServiceImpl implements MetaphysicsService{
	@Autowired
	private MetaphysicsMapper metaphysicsMapper;

	/**
	 * 将所给的字符串转化成编码
	 * @param str 待转化的字符串
	 * @return 转化后的编码
	 */
	@Override
	public String generateCode(String str) {
		StringBuilder res = new StringBuilder();
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (c == '-') {
				res.append("M");
			}
			else if (c == ':') {
				res.append("O");
			}
			else if (c == ' ') {
				res.append("S");
			}
			else if (c >= '0' && c <= '9') {
				res.append((char) (c + 17));
			}
			else {
				res.append(c);
			}
		}
		return res.toString();
	}

	/**
	 * 将所给编码还原为字符串
	 * @param code 待还原的编码
	 * @return 还原后的字符串
	 */
	@Override
	public String parseCode(String code) {
		StringBuilder res = new StringBuilder();
		for (int i = 0; i < code.length(); i++) {
			char c = code.charAt(i);
			if (c == 'M') {
				res.append("-");
			}
			else if (c == 'O') {
				res.append(":");
			}
			else if (c == 'S') {
				res.append(" ");
			}
			else if (c >= 'A' && c <= 'J') {
				res.append((char) (c - 17));
			}
			else {
				res.append(c);
			}
		}
		return res.toString();
	}

	@Override
	public Tone getToneByStemAndBranch(StemAndBranch stemAndBranch) {
		return metaphysicsMapper.getToneByStemIdAndBranchId(stemAndBranch.getStem().getId(), stemAndBranch.getBranch().getId());
	}

	@Override
	public String[] getTenGodsByStemAndBranches(StemAndBranch[] stemAndBranches) {
		String[] tenGods = new String[4];
		tenGods[2] = "日元";
		for (int i = 0; i < stemAndBranches.length; i++) {
			if (i != 2) {
				YinYang oppositeYinYang = stemAndBranches[i].getStem().getYinYang();
				YinYang myYinYang = stemAndBranches[2].getStem().getYinYang();
				Element oppositeElement = stemAndBranches[i].getStem().getElement();
				Element myElement = stemAndBranches[2].getStem().getElement();
				if (oppositeElement.getGeneration() == myElement.getId()) {
					// 生我者为枭印。
					if (oppositeYinYang == myYinYang) {
						// 同性为偏印，也叫枭神，简称枭。
						tenGods[i] = "枭神";
					}
					else {
						// 异性为正印，简称印。
						tenGods[i] = "正印";
					}
				}
				else if (myElement.getGeneration() == oppositeElement.getId()) {
					// 我生者为食伤。
					if (oppositeYinYang == myYinYang) {
						// 同性为食神，简称食。
						tenGods[i] = "食神";
					}
					else {
						// 异性为伤官，简称伤。
						tenGods[i] = "伤官";
					}
				}
				else if (myElement.getRestriction() == oppositeElement.getId()) {
					// 我克者为才财。
					if (oppositeYinYang == myYinYang) {
						// 同性为偏财，简称财。
						tenGods[i] = "偏财";
					}
					else {
						// 异性为正财，简称才。
						tenGods[i] = "正财";
					}
				}
				else if (oppositeElement.getRestriction() == myElement.getId()) {
					// 克我者为杀官。
					if (oppositeYinYang == myYinYang) {
						// 同性为偏官，或者七杀，简称杀。
						tenGods[i] = "七杀";
					}
					else {
						// 异性为正官，简称官。
						tenGods[i] = "正官";
					}
				}
				else {
					// 同我者为比劫。
					if (oppositeYinYang == myYinYang) {
						// 同性为比肩，简称比。
						tenGods[i] = "比肩";
					}
					else {
						// 异性为劫财，简称劫。
						tenGods[i] = "劫财";
					}
				}
			}
		}
		return tenGods;
	}

	@Override
	public Trigram getTrigramByYaos(Yao[] yaos) {
		for (Trigram trigram : MetaphysicsConstants.TRIGRAM_LIST) {
			if (Arrays.equals(trigram.getYaos(), yaos)) {
				return trigram;
			}
		}
		return null;
	}
}
