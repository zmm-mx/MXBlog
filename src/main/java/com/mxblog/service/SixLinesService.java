package com.mxblog.service;

import com.mxblog.pojo.Diagram;
import com.mxblog.pojo.Element;
import com.mxblog.vo.SixLinesVO;

public interface SixLinesService {
	SixLinesVO getSixLinesVO(int[] lines, String dateString);
	void calculateShiAndYing(Diagram diagram);
	void calculateSAB(Diagram diagram);
	void calculateSixRelations(Element oriElement, Diagram diagram);
	void calculateSixGods(int stemId, Diagram diagram);
	void calculateBodyBranch(Diagram diagram);
	String getCode(int[] lines, String dateString);
	String subCode(String code);
	String[] getLineAndDateTime(String code);
}
