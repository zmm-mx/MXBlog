package com.mxblog.service;

import com.mxblog.pojo.Blog;
import com.mxblog.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class ViewServiceImpl implements ViewService {
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private BlogServiceImpl blogService;

    @Override
    //博客浏览量+1
    public int updateBlogViews(int id){
        long incr = redisUtil.incr(blogService.queryBlogById(id).getRedisKeyWithId(), 1);
        return (int) incr;
    }

    @Override
    //排行榜内指定博客浏览量+1
    public Double updateBlogViewsInRank(int id){
        Double result = redisUtil.ZIncrBy("mxblog:rank:view", id, 1);
        return result;
    }

    //浏览量从Redis写入MySQL
    @Override
    @Scheduled(cron = "0 0 4 * * ?")    //每天凌晨4点触发
    public void saveBlogViewsFromRedis() {
        List<Blog> blogIds = blogService.queryAllBlogIds();
        for (Blog blogId : blogIds) {
            Object key = redisUtil.get(blogId.getRedisKeyWithId());
            if (key != null) {
                HashMap<String, Object> map = new HashMap<>();
                map.put("id", blogId.getId());
                map.put("views", (Integer) key);
                blogService.updateBlogViewsWithRedis(map);
            }
        }
    }

    //重置排行榜中所有博客当日的点击量为0
    @Override
    @Scheduled(cron = "0 0 4 * * ?")    //每天凌晨4点触发
    public void clearViewRank() {
        List<Blog> blogs = blogService.queryAllBlogIds();
        for (Blog blog : blogs) {
            redisUtil.ZAdd("mxblog:rank:view", blog.getId(), 0);
        }
    }
}
