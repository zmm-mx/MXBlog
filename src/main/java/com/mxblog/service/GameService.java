package com.mxblog.service;

import com.mxblog.pojo.Game;

import java.util.List;

public interface GameService {
	// 获取游戏作品的数量
	int queryCountsOfGames();
	// 获取所有的游戏作品的信息
	List<Game> queryAllGames();
}
