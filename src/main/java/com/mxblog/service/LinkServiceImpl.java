package com.mxblog.service;

import com.mxblog.mapper.LinkMapper;
import com.mxblog.pojo.Link;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class LinkServiceImpl implements LinkService{
    @Autowired
    private LinkMapper mapper;

    //分页查询友链
    @Override
    public List<Link> queryLinkByPage(Map<String, Integer> pageInfo) {
        return mapper.queryLinkByPage(pageInfo);
    }

    //统计友链总量
    @Override
    public int countLink() {
        return mapper.countLink();
    }

    //根据id查询友链是第几条
    @Override
    public int countLinkById(int id) {
        return mapper.countLinkById(id);
    }

    //根据id删除友链
    @Override
    public int deleteLinkById(int id) {
        return mapper.deleteLinkById(id);
    }

    //根据博客地址查询友链
    @Override
    public Link queryLinkByBlogLink(String blogLink) {
        return mapper.queryLinkByBlogLink(blogLink);
    }

    //添加一个友链
    @Override
    public int insertLink(Link link) {
        return mapper.insertLink(link);
    }

    //按照id查询单个友链
    @Override
    public Link queryLinkById(int id) {
        return mapper.queryLinkById(id);
    }

    //修改一个友链
    @Override
    public int updateLink(Link link) {
        return mapper.updateLink(link);
    }
}
