package com.mxblog.service;

import com.mxblog.mapper.GameMapper;
import com.mxblog.pojo.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GameServiceImpl implements GameService{
	@Autowired
	private GameMapper mapper;

	@Override
	public int queryCountsOfGames() {
		return mapper.queryCountsOfGames();
	}

	@Override
	public List<Game> queryAllGames() {
		return mapper.queryAllGames();
	}
}
