package com.mxblog.service;

import com.mxblog.mapper.BlogColumnMapper;
import com.mxblog.pojo.BlogColumn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class BlogColumnServiceImpl implements BlogColumnService {
    @Autowired
    private BlogColumnMapper mapper;
    //分页查询博客专栏
    @Override
    public List<BlogColumn> queryBlogColumnByPage(Map<String, Integer> pageInfo) {
        return mapper.queryBlogColumnByPage(pageInfo);
    }
    //获取全部博客专栏
    @Override
    public List<BlogColumn> queryAllBlogColumns() {
        return mapper.queryAllBlogColumns();
    }
    //统计专栏总量
    @Override
    public int countBlogColumn() {
        return mapper.countBlogColumn();
    }
    //根据id统计数量
    @Override
    public int countBlogColumnById(int id) {
        return mapper.countBlogColumnById(id);
    }
    //根据名称查询专栏
    @Override
    public BlogColumn queryBlogColumnByName(String name) {
        return mapper.queryBlogColumnByName(name);
    }
    //按照id查询单个专栏
    @Override
    public BlogColumn queryBlogColumnById(int id) {
        return mapper.queryBlogColumnById(id);
    }
    //添加一个专栏
    @Override
    public int insertBlogColumn(BlogColumn blogColumn) {
        return mapper.insertBlogColumn(blogColumn);
    }
    //根据id删除一个专栏
    @Override
    public int deleteBlogColumnById(int id) {
        return mapper.deleteBlogColumnById(id);
    }
    //修改一个专栏
    @Override
    public int updateBlogColumn(BlogColumn blogColumn) {
        return mapper.updateBlogColumn(blogColumn);
    }
}
