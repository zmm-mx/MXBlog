package com.mxblog.service;

import com.mxblog.mapper.ArchivesMapper;
import com.mxblog.pojo.Archives;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArchivesServiceImpl implements ArchivesService{
    @Autowired
    private ArchivesMapper mapper;

    @Override
    //获取所有的年份
    public List<String> queryAllYearsWithoutShield(){
        return mapper.queryAllYearsWithoutShield();
    }
    @Override
    //按照年份获取所有未被屏蔽的博客
    public List<Archives> queryBlogsByYearWithoutShield(String year){
        return mapper.queryBlogsByYearWithoutShield(year);
    }
}
