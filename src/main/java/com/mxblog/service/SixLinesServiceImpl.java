package com.mxblog.service;

import com.mxblog.constants.MetaphysicsConstants;
import com.mxblog.mapper.MetaphysicsMapper;
import com.mxblog.pojo.*;
import com.mxblog.utils.DateStemBranchUtils;
import com.mxblog.vo.SixLinesVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SixLinesServiceImpl implements SixLinesService {
	@Autowired
	private MetaphysicsService metaphysicsService;
	@Autowired
	private MetaphysicsMapper metaphysicsMapper;

	@Override
	public SixLinesVO getSixLinesVO(int[] lines, String dateString) {
		SixLinesVO vo = new SixLinesVO();
		// 计算四柱
		StemAndBranch[] stemAndBranches = DateStemBranchUtils.calStemBranch(dateString);
		// 六爻主体
		SixLines sixLines = new SixLines();
		// 分别计算本卦与变卦的上下卦
		Yao[] yaosOriginalTop = new Yao[3];
		Yao[] yaosOriginalBottom = new Yao[3];
		Yao[] yaosChangingTop = new Yao[3];
		Yao[] yaosChangingBottom = new Yao[3];
		for (int i = 0, j = 2; i <= 2 && j >= 0; i++, j--) {
			int line = lines[i];
			switch (line) {
				case 0:
					yaosOriginalBottom[j] = MetaphysicsConstants.YANG_YAO;
					yaosChangingBottom[j] = MetaphysicsConstants.YIN_YAO;
					break;
				case 1:
					yaosOriginalBottom[j] = MetaphysicsConstants.YIN_YAO;
					yaosChangingBottom[j] = MetaphysicsConstants.YIN_YAO;
					break;
				case 2:
					yaosOriginalBottom[j] = MetaphysicsConstants.YANG_YAO;
					yaosChangingBottom[j] = MetaphysicsConstants.YANG_YAO;
					break;
				case 3:
					yaosOriginalBottom[j] = MetaphysicsConstants.YIN_YAO;
					yaosChangingBottom[j] = MetaphysicsConstants.YANG_YAO;
					break;
				default:
					break;
			}
		}
		for (int i = 3, j = 2; i <= 5 && j >= 0; i++, j--) {
			int line = lines[i];
			switch (line) {
				case 0:
					yaosOriginalTop[j] = MetaphysicsConstants.YANG_YAO;
					yaosChangingTop[j] = MetaphysicsConstants.YIN_YAO;
					break;
				case 1:
					yaosOriginalTop[j] = MetaphysicsConstants.YIN_YAO;
					yaosChangingTop[j] = MetaphysicsConstants.YIN_YAO;
					break;
				case 2:
					yaosOriginalTop[j] = MetaphysicsConstants.YANG_YAO;
					yaosChangingTop[j] = MetaphysicsConstants.YANG_YAO;
					break;
				case 3:
					yaosOriginalTop[j] = MetaphysicsConstants.YIN_YAO;
					yaosChangingTop[j] = MetaphysicsConstants.YANG_YAO;
					break;
				default:
					break;
			}
		}
		Trigram trigramOriginalTop = metaphysicsService.getTrigramByYaos(yaosOriginalTop);
		Trigram trigramOriginalBottom = metaphysicsService.getTrigramByYaos(yaosOriginalBottom);
		Trigram trigramChangingTop = metaphysicsService.getTrigramByYaos(yaosChangingTop);
		Trigram trigramChangingBottom = metaphysicsService.getTrigramByYaos(yaosChangingBottom);
		// 计算对应的六十四卦
		int originalId = (trigramOriginalTop.getId() - 1) * 8 + trigramOriginalBottom.getId();
		Diagram diagramOriginal = metaphysicsMapper.getDiagramById(originalId);
		int changingId = (trigramChangingTop.getId() - 1) * 8 + trigramChangingBottom.getId();
		Diagram diagramChanging = metaphysicsMapper.getDiagramById(changingId);
		// 设置上下卦
		diagramOriginal.setTrigrams(new Trigram[]{trigramOriginalTop, trigramOriginalBottom});
		diagramChanging.setTrigrams(new Trigram[]{trigramChangingTop, trigramChangingBottom});
		// 设置类型
		diagramOriginal.setType("本卦");
		diagramChanging.setType("变卦");
		// 计算世爻、应爻
		calculateShiAndYing(diagramOriginal);
		calculateShiAndYing(diagramChanging);
		// 计算纳甲
		calculateSAB(diagramOriginal);
		calculateSAB(diagramChanging);
		// 计算六亲
		calculateSixRelations(MetaphysicsConstants.TRIGRAM_LIST[diagramOriginal.getPalace()-1].getElement(), diagramOriginal);
		calculateSixRelations(MetaphysicsConstants.TRIGRAM_LIST[diagramOriginal.getPalace()-1].getElement(), diagramChanging);
		// 计算六神
		calculateSixGods(stemAndBranches[2].getStem().getId(), diagramOriginal);
		calculateSixGods(stemAndBranches[2].getStem().getId(), diagramChanging);
		// 计算卦身
		calculateBodyBranch(diagramOriginal);
		calculateBodyBranch(diagramChanging);
		// 设置本卦和变卦
		sixLines.setDiagrams(new Diagram[]{diagramOriginal, diagramChanging});
		// 设置六爻
		vo.setSixLines(sixLines);
		// 设置六次摇卦结果
		vo.setLinesArray(lines);
		// 设置日期
		vo.setDateTime(dateString);
		// 设置四柱
		StringBuilder lunarStr = new StringBuilder();
		for (StemAndBranch sab : stemAndBranches) {
			lunarStr.append(sab.getStem().getName());
			lunarStr.append(sab.getBranch().getName());
			lunarStr.append(" ");
		}
		vo.setDateTimeLunar(lunarStr.toString());
		// 设置卦例编码
		vo.setCode(getCode(lines, dateString));
		return vo;
	}

	@Override
	public void calculateShiAndYing(Diagram diagram) {
		diagram.setPalaceName(MetaphysicsConstants.TRIGRAM_LIST[diagram.getPalace() - 1].getName());
		int p = diagram.getPosition();
		switch (p) {
			case 1:
				diagram.setShiYaoNum(0);
				diagram.setYingYaoNum(3);
				diagram.setShiYaoName("八纯卦");
				break;
			case 2:
				diagram.setShiYaoNum(5);
				diagram.setYingYaoNum(2);
				diagram.setShiYaoName("一世卦");
				break;
			case 3:
				diagram.setShiYaoNum(4);
				diagram.setYingYaoNum(1);
				diagram.setShiYaoName("二世卦");
				break;
			case 4:
				diagram.setShiYaoNum(3);
				diagram.setYingYaoNum(0);
				diagram.setShiYaoName("三世卦");
				break;
			case 5:
				diagram.setShiYaoNum(2);
				diagram.setYingYaoNum(5);
				diagram.setShiYaoName("四世卦");
				break;
			case 6:
				diagram.setShiYaoNum(1);
				diagram.setYingYaoNum(4);
				diagram.setShiYaoName("五世卦");
				break;
			case 7:
				diagram.setShiYaoNum(2);
				diagram.setYingYaoNum(5);
				diagram.setShiYaoName("游魂卦");
				break;
			case 8:
				diagram.setShiYaoNum(3);
				diagram.setYingYaoNum(0);
				diagram.setShiYaoName("归魂卦");
				break;
			default:
				break;
		}
	}

	/*
		纳甲口诀：
		乾金甲子外壬午，坎水戊寅外戊申，
		艮土丙辰外丙戌，震木庚子外庚午，
		巽木辛丑外辛未，离火已卯外已酉，
		坤土乙未外癸丑，兑金丁巳外丁亥。
	 */
	@Override
	public void calculateSAB(Diagram diagram) {
		Trigram[] trigrams = diagram.getTrigrams();
		Trigram innerTrigram = trigrams[1];
		Trigram outerTrigram = trigrams[0];
		StemAndBranch[] storageJia = new StemAndBranch[6];
		int innerStemNum = 0;
		int innerBranchNum = 0;
		int outerStemNum = 0;
		int outerBranchNum = 0;
		switch (innerTrigram.getId()) {
			case 1:
				innerStemNum = 1;
				innerBranchNum = 1;
				break;
			case 2:
				innerStemNum = 4;
				innerBranchNum = 6;
				break;
			case 3:
				innerStemNum = 6;
				innerBranchNum = 4;
				break;
			case 4:
				innerStemNum = 7;
				innerBranchNum = 1;
				break;
			case 5:
				innerStemNum = 8;
				innerBranchNum = 2;
				break;
			case 6:
				innerStemNum = 5;
				innerBranchNum = 3;
				break;
			case 7:
				innerStemNum = 3;
				innerBranchNum = 5;
				break;
			case 8:
				innerStemNum = 2;
				innerBranchNum = 8;
				break;
			default:
				break;
		}
		switch (outerTrigram.getId()) {
			case 1:
				outerStemNum = 9;
				outerBranchNum = 7;
				break;
			case 2:
				outerStemNum = 4;
				outerBranchNum = 12;
				break;
			case 3:
				outerStemNum = 6;
				outerBranchNum = 10;
				break;
			case 4:
				outerStemNum = 7;
				outerBranchNum = 7;
				break;
			case 5:
				outerStemNum = 8;
				outerBranchNum = 8;
				break;
			case 6:
				outerStemNum = 5;
				outerBranchNum = 9;
				break;
			case 7:
				outerStemNum = 3;
				outerBranchNum = 11;
				break;
			case 8:
				outerStemNum = 10;
				outerBranchNum = 2;
				break;
			default:
				break;
		}
		for (int i = 2; i >= 0; i--) {
			storageJia[i+3] = new StemAndBranch(MetaphysicsConstants.STEM_LIST[innerStemNum - 1], MetaphysicsConstants.BRANCH_LIST[innerBranchNum - 1]);
			storageJia[i] = new StemAndBranch(MetaphysicsConstants.STEM_LIST[outerStemNum - 1], MetaphysicsConstants.BRANCH_LIST[outerBranchNum - 1]);
			if (innerStemNum % 2 == 1) {
				innerBranchNum += 2;
				if (innerBranchNum > 12) {
					innerBranchNum -= 12;
				}
			}
			else {
				innerBranchNum -= 2;
				if (innerBranchNum < 1) {
					innerBranchNum += 12;
				}
			}
			if (outerStemNum % 2 == 1) {
				outerBranchNum += 2;
				if (outerBranchNum > 12) {
					outerBranchNum -= 12;
				}
			}
			else {
				outerBranchNum -= 2;
				if (outerBranchNum < 1) {
					outerBranchNum += 12;
				}
			}
		}
		diagram.setStorageJia(storageJia);
	}

	@Override
	public void calculateSixRelations(Element oriElement, Diagram diagram) {
		String[] sixRelations = new String[6];
		StemAndBranch[] storageJia = diagram.getStorageJia();
		for (int i = 0; i < storageJia.length; i++) {
			Element yaoElement = storageJia[i].getBranch().getElement();
			if (yaoElement.getGeneration() == oriElement.getId()) {
				sixRelations[i] = "父母";
			}
			else if (yaoElement.getRestriction() == oriElement.getId()) {
				sixRelations[i] = "官鬼";
			}
			else if (oriElement.getGeneration() == yaoElement.getId()) {
				sixRelations[i] = "子孙";
			}
			else if (oriElement.getRestriction() == yaoElement.getId()) {
				sixRelations[i] = "妻财";
			}
			else {
				sixRelations[i] = "兄弟";
			}
		}
		diagram.setSixRelations(sixRelations);
	}

	/*
		甲乙起青龙
		丙丁起朱雀
		戊日起勾陈
		己日起媵蛇
		庚辛起白虎
		壬癸起玄武
		从下依次装
	 */
	@Override
	public void calculateSixGods(int stemId, Diagram diagram) {
		int startIndex = 0;
		if (stemId <= 4) {
			startIndex = (stemId - 1) / 2;
		}
		else if (stemId <= 6) {
			startIndex = stemId - 3;
		}
		else {
			startIndex = (stemId + 1) / 2;
		}
		String[] sixGods = new String[6];
		for (int i = 5; i >= 0; i--) {
			sixGods[i] = MetaphysicsConstants.SIX_GOD_LIST[startIndex++];
			if (startIndex >= 6) {
				startIndex -= 6;
			}
		}
		diagram.setSixGods(sixGods);
	}

	/*
		阳世从子数，阴世从午数，
		数到世爻，即是卦身。
	 */
	@Override
	public void calculateBodyBranch(Diagram diagram) {
		int branchNum = 0;
		int shiYaoNum = diagram.getShiYaoNum();
		int yaoNumInTrigram = 0;
		if (shiYaoNum <= 2) {
			yaoNumInTrigram = shiYaoNum;
		}
		else {
			yaoNumInTrigram = shiYaoNum - 3;
		}
		if (diagram.getTrigrams()[1].getYaos()[yaoNumInTrigram].getYinYang() == MetaphysicsConstants.YANG) {
			branchNum += (5 - shiYaoNum);
		}
		else {
			branchNum += (11 - shiYaoNum);
		}
		Branch bodyBranch = MetaphysicsConstants.BRANCH_LIST[branchNum];
		diagram.setBodyBranch(bodyBranch);
	}

	@Override
	public String getCode(int[] lines, String dateString) {
		StringBuilder sb = new StringBuilder();
		for (int line : lines) {
			sb.append(line);
		}
		sb.append(dateString);
		return "SL-" + metaphysicsService.generateCode(sb.toString());
	}

	@Override
	public String subCode(String code) {
		return code.substring(3);
	}

	@Override
	public String[] getLineAndDateTime(String code) {
		code = metaphysicsService.parseCode(code);
		String[] res = new String[7];
		for (int i = 0; i < 6; i++) {
			res[i] = String.valueOf(code.charAt(i));
		}
		res[6] = code.substring(6);
		return res;
	}
}
