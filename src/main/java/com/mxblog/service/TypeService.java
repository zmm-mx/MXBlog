package com.mxblog.service;

import com.mxblog.pojo.Type;

import java.util.List;

public interface TypeService {
    //获取所有类别
    public List<Type> getAllTypes();
    //根据Id获取类别
    public Type getTypeById(int id);
}
