package com.mxblog.service;

import com.mxblog.mapper.SearchMapper;
import com.mxblog.pojo.Blog;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SearchServiceImpl implements SearchService{
    @Autowired
    private SearchMapper mapper;

    // 创建索引
    public void createIndex() {
        System.out.println("blog 索引创建成功。");
    }

    // 把博客对象存入ES中
    @Override
    public void insertBlog(Blog blog) {
        mapper.save(blog);
    }

    // 批量把博客对象存入ES中
    @Override
    public void insertBlogList(List<Blog> blogList) {
        mapper.saveAll(blogList);
    }

    // 分页查询博客
    @Override
    public List<Blog> findBlogByTitleWithPage(String title, int from, int size) {
        // 设置排序方式为按照id降序排列
        Sort sort = Sort.by(Sort.Direction.DESC, "id");
        // 设置查询分页
        PageRequest pageRequest = PageRequest.of(from, size, sort);
        // 设置查询条件
        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("title", title);
        // 分页查询
        Page<Blog> blogs = mapper.search(termQueryBuilder, pageRequest);
        // 返回查询结果
        ArrayList<Blog> blogList = new ArrayList<>();
        for (Blog blog : blogs) {
            blogList.add(blog);
        }
        return blogList;
    }
}
