package com.mxblog.service;

import com.mxblog.pojo.Link;

import java.util.List;
import java.util.Map;

public interface LinkService {
    //分页查询友链
    List<Link> queryLinkByPage(Map<String, Integer> pageInfo);
    //统计友链总量
    int countLink();
    //根据id查询友链是第几条
    int countLinkById(int id);
    //根据id删除一条友链
    int deleteLinkById(int id);
    //根据博客地址查询友链
    Link queryLinkByBlogLink(String blogLink);
    //添加一个友链
    int insertLink(Link link);
    //按照id查询单个友链
    Link queryLinkById(int id);
    //修改一个友链
    int updateLink(Link link);
}
