package com.mxblog.service;

import com.mxblog.constants.MetaphysicsConstants;
import com.mxblog.mapper.MetaphysicsMapper;
import com.mxblog.pojo.*;
import com.mxblog.utils.CharacterUtils;
import com.mxblog.utils.DateStemBranchUtils;
import com.mxblog.vo.PlumBlossomVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.*;

@Service
public class PlumBlossomServiceImpl implements PlumBlossomService{
	@Autowired
	private MetaphysicsMapper metaphysicsMapper;
	@Autowired
	private MetaphysicsService metaphysicsService;

	@Override
	public PlumBlossomVO getPlumBlossomVO(String ch1, String ch2, String dateString) throws UnsupportedEncodingException {
		PlumBlossomVO vo = new PlumBlossomVO();

		// 汉字转笔画数，先将 UTF-8 转化成 GBK
		byte[] bytes1 = ch1.getBytes("GBK");
		byte[] bytes2 = ch2.getBytes("GBK");
		int num1 = CharacterUtils.calculateNumberOfStrokes(bytes1);
		int num2 = CharacterUtils.calculateNumberOfStrokes(bytes2);

		// 计算四柱
		StemAndBranch[] stemAndBranches = DateStemBranchUtils.calStemBranch(dateString);

		List<Diagram> diagrams = new ArrayList<>();
		// 本卦
		// 计算两个卦象
		Trigram t1_origin = calculateTrigram(num1);
		Trigram t2_origin = calculateTrigram(num2);
		// 计算编号
		int id_origin = (t1_origin.getId() - 1) * 8 + t2_origin.getId();
		// 查询本卦
		Diagram diagram_original = metaphysicsMapper.getDiagramById(id_origin);
		// 设置类型为本卦
		diagram_original.setType("本卦");
		// 设置上下卦
		diagram_original.setTrigrams(new Trigram[]{t1_origin, t2_origin});
		diagrams.add(diagram_original);

		// 互卦
		// 获取两个卦象
		Yao[] yaos1_origin = t1_origin.getYaos();
		Yao[] yaos2_origin = t2_origin.getYaos();
		Yao[] yaos1_interactive = new Yao[]{yaos1_origin[1], yaos1_origin[2], yaos2_origin[0]};
		Yao[] yaos2_interactive = new Yao[]{yaos1_origin[2], yaos2_origin[0], yaos2_origin[1]};
		Trigram t1_interactive = metaphysicsService.getTrigramByYaos(yaos1_interactive);
		Trigram t2_interactive = metaphysicsService.getTrigramByYaos(yaos2_interactive);
		// 计算编号
		int id_interactive = (t1_interactive.getId() - 1) * 8 + t2_interactive.getId();
		// 查询互卦
		Diagram diagram_interactive = metaphysicsMapper.getDiagramById(id_interactive);
		// 设置类型为互卦
		diagram_interactive.setType("互卦");
		// 设置上下卦
		diagram_interactive.setTrigrams(new Trigram[]{t1_interactive, t2_interactive});
		diagrams.add(diagram_interactive);

		// 变卦
		// 确定动爻
		int change = num1 + num2 + stemAndBranches[3].getBranch().getId();
		change = change % 6;
		if (change == 0) {
			change = 6;
		}
		// 获取两个卦象
		Trigram t1_changing = null;
		Trigram t2_changing = null;
		if (change <= 3) {
			// 下卦动
			t1_changing = t1_origin;
			t2_changing = getTrigramWithChanging(t2_origin, change);
		}
		else {
			// 上卦动
			t1_changing = getTrigramWithChanging(t1_origin, change - 3);
			t2_changing = t2_origin;
		}
		// 计算编号
		int id_changing = (t1_changing.getId() - 1) * 8 + t2_changing.getId();
		// 查询变卦
		Diagram diagram_changing = metaphysicsMapper.getDiagramById(id_changing);
		// 设置类型为变卦
		diagram_changing.setType("变卦");
		// 设置上下卦
		diagram_changing.setTrigrams(new Trigram[]{t1_changing, t2_changing});
		diagrams.add(diagram_changing);

		// 计算体卦与用卦
		int body = (change - 1) / 3;
		int using = body ^ 1;
		// 设置吉凶
		diagram_original.setLuck(getLuck(diagram_original.getTrigrams()[body], diagram_original.getTrigrams()[using]));
		diagram_interactive.setLuck(
				getLuck(diagram_original.getTrigrams()[body], diagram_interactive.getTrigrams()[0]) + " / " +
				getLuck(diagram_original.getTrigrams()[body], diagram_interactive.getTrigrams()[1])
		);
		diagram_changing.setLuck(getLuck(diagram_original.getTrigrams()[body], diagram_changing.getTrigrams()[using]));

		// 封装结果
		// 设置双字
		vo.setCharacter1(ch1);
		vo.setCharacter2(ch2);
		// 设置日期
		vo.setDate(dateString);
		// 设置四柱
		StringBuilder lunarStr = new StringBuilder();
		for (StemAndBranch sab : stemAndBranches) {
			lunarStr.append(sab.getStem().getName());
			lunarStr.append(sab.getBranch().getName());
			lunarStr.append(" ");
		}
		vo.setDateLunar(lunarStr.toString());
		// 设置三个卦象
		vo.setDiagrams(diagrams);
		// 设置旺衰
		vo.setSituation(getSituation(diagram_original.getTrigrams()[body].getElement(), stemAndBranches[1].getBranch().getElement()));
		// 设置卦例编码
		vo.setCode(getCode(ch1, ch2, dateString));
		return vo;
	}

	@Override
	public Trigram calculateTrigram(int num) {
		num = num % 8;
		if (num == 0) {
			num = 8;
		}
		Trigram trigram = null;
		switch (num) {
			case 1:
				trigram = MetaphysicsConstants.TR_HEAVEN;
				break;
			case 2:
				trigram = MetaphysicsConstants.TR_LAKE;
				break;
			case 3:
				trigram = MetaphysicsConstants.TR_FIRE;
				break;
			case 4:
				trigram = MetaphysicsConstants.TR_THUNDER;
				break;
			case 5:
				trigram = MetaphysicsConstants.TR_WIND;
				break;
			case 6:
				trigram = MetaphysicsConstants.TR_WATER;
				break;
			case 7:
				trigram = MetaphysicsConstants.TR_MOUNTAIN;
				break;
			case 8:
				trigram = MetaphysicsConstants.TR_EARTH;
				break;
			default:
				break;
		}
		return trigram;
	}

	@Override
	public Trigram getTrigramWithChanging(Trigram origin, int changing) {
		Yao[] yaos = origin.getYaos();
		List<Yao> yaoList = new ArrayList<>(Arrays.asList(yaos));
		Collections.reverse(yaoList);
		if (yaoList.get(changing - 1).equals(MetaphysicsConstants.YIN_YAO)) {
			yaoList.set(changing - 1, MetaphysicsConstants.YANG_YAO);
		}
		else {
			yaoList.set(changing - 1, MetaphysicsConstants.YIN_YAO);
		}
		Collections.reverse(yaoList);
		return metaphysicsService.getTrigramByYaos(yaoList.toArray(new Yao[0]));
	}

	@Override
	public String getLuck(Trigram bT, Trigram uT) {
		if (bT.getElement().getGeneration() == uT.getElement().getId()) {
			return "体生用<strong>小凶</strong>";
		}
		else if (uT.getElement().getGeneration() == bT.getElement().getId()) {
			return "用生体<strong>大吉</strong>";
		}
		else if (bT.getElement().getRestriction() == uT.getElement().getId()) {
			return "体克用<strong>中平</strong>";
		}
		else if (uT.getElement().getRestriction() == bT.getElement().getId()) {
			return "用克体<strong>大凶</strong>";
		}
		else if (uT.getElement().getId() == bT.getElement().getId()) {
			return "体用比<strong>小吉</strong>";
		}
		return null;
	}

	@Override
	public String getSituation(Element bodyElement, Element monthElement) {
		if (bodyElement.getGeneration() == monthElement.getId()) {
			return "休";
		}
		else if (monthElement.getGeneration() == bodyElement.getId()) {
			return "相";
		}
		else if (bodyElement.getRestriction() == monthElement.getId()) {
			return "囚";
		}
		else if (monthElement.getRestriction() == bodyElement.getId()) {
			return "死";
		}
		else if (monthElement.getId() == bodyElement.getId()) {
			return "旺";
		}
		return null;
	}

	@Override
	public String getCode(String ch1, String ch2, String dateString) throws UnsupportedEncodingException {
		byte[] bytes1 = ch1.getBytes("GBK");
		byte[] bytes2 = ch2.getBytes("GBK");
		StringBuilder sb = new StringBuilder();
		sb.append(bytes1[0]);
		sb.append("T");
		sb.append(bytes1[1]);
		sb.append("K");
		sb.append(bytes2[0]);
		sb.append("T");
		sb.append(bytes2[1]);
		sb.append("K");
		sb.append(dateString);
		return "PB-" + metaphysicsService.generateCode(sb.toString());
	}

	@Override
	public String subCode(String code) {
		if (code.startsWith("PB-")) {
			code = code.substring(3);
		}
		return code;
	}

	@Override
	public List<String> getCharactersAndTime(String code) throws UnsupportedEncodingException {
		List<String> res = new ArrayList<>(3);
		String[] codes = code.split("K");
		String ch1 = metaphysicsService.parseCode(codes[0]);
		String ch2 = metaphysicsService.parseCode(codes[1]);
		String dateString = metaphysicsService.parseCode(codes[2]);
		String[] bytes1Str = ch1.split("T");
		byte[] bytes1 = new byte[]{Byte.parseByte(bytes1Str[0]), Byte.parseByte(bytes1Str[1])};
		String[] bytes2Str = ch2.split("T");
		byte[] bytes2 = new byte[]{Byte.parseByte(bytes2Str[0]), Byte.parseByte(bytes2Str[1])};
		ch1 = new String(bytes1, "GBK");
		ch2 = new String(bytes2, "GBK");
		res.add(ch1);
		res.add(ch2);
		res.add(dateString);
		return res;
	}
}
