package com.mxblog.service;

public interface ViewService {
    //博客浏览量+1
    public int updateBlogViews(int id);
    //排行榜内指定博客浏览量+1
    public Double updateBlogViewsInRank(int id);
    //浏览量从Redis写入MySQL
    public void saveBlogViewsFromRedis();
    //重置排行榜中所有博客当日的点击量为0
    public void clearViewRank();
}
