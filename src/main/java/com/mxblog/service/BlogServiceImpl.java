package com.mxblog.service;

import com.mxblog.mapper.BlogMapper;
import com.mxblog.pojo.Blog;
import com.mxblog.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class BlogServiceImpl implements BlogService {
    @Autowired
    private BlogMapper mapper;
    @Autowired
    private RedisUtil redisUtil;
    //分页查询博客
    @Override
    public List<Blog> queryBlogByPage(Map<String, Object> pageInfo) {
        return mapper.queryBlogByPage(pageInfo);
    }
    //统计博客总量
    @Override
    public int countBlog(Map<String, Object> numInfo) {
        return mapper.countBlog(numInfo);
    }
    //按照id查询单个博客
    @Override
    public Blog queryBlogById(int id) {
        return mapper.queryBlogById(id);
    }
    //添加一个博客
    @Override
    public int insertBlog(Blog blog) {
        return mapper.insertBlog(blog);
    }
    //添加博客与标签的联系
    @Override
    public int insertBlogAndTag(Blog blog) {
        return mapper.insertBlogAndTag(blog);
    }
    //删除博客与标签的联系
    @Override
    public int deleteBlogAndTagById(int id) {
        return mapper.deleteBlogAndTagById(id);
    }

    //根据id删除一个博客
    @Override
    public int deleteBlogById(int id) {
        return mapper.deleteBlogById(id);
    }
    //统计当前博客是第几条
    @Override
    public int countBlogById(int id) {
        return mapper.countBlogById(id);
    }
    //修改一个博客
    @Override
    public int updateBlog(Blog blog) {
        return mapper.updateBlog(blog);
    }
    //根据id屏蔽/解除一个博客
    @Override
    public int shieldBlog(Map<String, Integer> shieldInfo) {
        return mapper.shieldBlog(shieldInfo);
    }
    //查询每个专栏的博客数量
    @Override
    public int queryBlogSumByColumn(int columnId) {
        return mapper.queryBlogSumByColumn(columnId);
    }
    //查询每个标签的博客数量
    @Override
    public int queryBlogSumByTag(int tagId) {
        return mapper.queryBlogSumByTag(tagId);
    }
    @Override
    //查询热度最高的前十个博客
    public List<Blog> getBlogsByViews(Map<String, Object> pageInfo) {
        return mapper.getBlogsByViews(pageInfo);
    }
    @Override
    //博客浏览量+1
    public int updateBlogViews(int id){
        return mapper.updateBlogViews(id);
    }
    @Override
    //根据id获取分页后的博客列表
    public List<Blog> getBlogsByPageWithColumnIdWithoutShield(Map<String, Object> map){
        return mapper.getBlogsByPageWithColumnIdWithoutShield(map);
    }
    @Override
    //根据专栏id统计未被屏蔽的博客的数量
    public int countBlogWithColumnIdWithoutShield(int id){
        return mapper.countBlogWithColumnIdWithoutShield(id);
    }
    @Override
    //查询每个专栏下未被屏蔽的博客数量
    public int queryBlogSumByColumnIdWithoutShield(int columnId){
        return mapper.queryBlogSumByColumnIdWithoutShield(columnId);
    }
    @Override
    //获取分页后的未被屏蔽的博客列表
    public List<Blog> queryBlogByPageWithoutShield(Map<String, Object> map){
        return mapper.queryBlogByPageWithoutShield(map);
    }
    @Override
    //获取未被屏蔽的博客总数
    public int countBlogWithoutShield(){
        return mapper.countBlogWithoutShield();
    }
    @Override
    //根据热度获取前十个未被屏蔽的博客
    public List<Blog> getBlogsByViewsWithoutShield(Map<String, Object> pageInfo){
        return mapper.getBlogsByViewsWithoutShield(pageInfo);
    }
    @Override
    //查询每个标签下未被屏蔽的博客数量
    public int queryBlogSumByTagIdWithoutShield(int tagId){
        return mapper.queryBlogSumByTagIdWithoutShield(tagId);
    }
    @Override
    //根据标签id获取分页后未被屏蔽的博客列表
    public List<Blog> getBlogsByPageWithTagIdWithoutShield(Map<String, Object> map){
        return mapper.getBlogsByPageWithTagIdWithoutShield(map);
    }
    @Override
    //查询所有博客
    public List<Blog> queryAllBlogs(){
        return mapper.queryAllBlogs();
    }
    //查询所有博客的id
    public List<Blog> queryAllBlogIds(){
        return mapper.queryAllBlogIds();
    }
    @Override
    //查询置顶博客
    public List<Blog> getTopBlogWithoutShield(){
        return mapper.getTopBlogWithoutShield();
    }
    @Override
    //获取分页后未被屏蔽且没有被置顶的博客列表
    public List<Blog> queryBlogByPageWithoutShieldAndNotTop(Map<String, Object> map){
        return mapper.queryBlogByPageWithoutShieldAndNotTop(map);
    }

    //根据博客id获取屏蔽结果
    @Override
    public int queryShieldByBlogId(int id) {
        return mapper.queryShieldByBlogId(id);
    }

    //根据博客id获取博客标题和id
    @Override
    public Blog queryBlogTitleAndIdByBlogId(int id) {
        return mapper.queryBlogTitleAndIdByBlogId(id);
    }

    //更新浏览量为Redis里的值
    @Override
    public int updateBlogViewsWithRedis(Map<String, Object> map) {
        return mapper.updateBlogViewsWithRedis(map);
    }

    //查询所有博客的id和view
    @Override
    public List<Blog> queryAllBlogIdsAndViews(){
        return mapper.queryAllBlogIdsAndViews();
    }
}
