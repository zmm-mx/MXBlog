package com.mxblog.service;

import com.mxblog.pojo.Archives;

import java.util.List;

public interface ArchivesService {
    //获取所有的年份
    public List<String> queryAllYearsWithoutShield();
    //按照年份获取所有未被屏蔽的博客
    public List<Archives> queryBlogsByYearWithoutShield(String year);
}
