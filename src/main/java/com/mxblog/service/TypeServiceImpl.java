package com.mxblog.service;

import com.mxblog.mapper.TypeMapper;
import com.mxblog.pojo.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeServiceImpl implements TypeService {
    @Autowired
    private TypeMapper mapper;

    //获取所有类别
    @Override
    public List<Type> getAllTypes() {
        return mapper.getAllTypes();
    }
    //根据Id获取类别
    public Type getTypeById(int id) {
        return mapper.getTypeById(id);
    }
}
