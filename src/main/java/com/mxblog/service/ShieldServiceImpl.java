package com.mxblog.service;

import com.mxblog.mapper.ShieldMapper;
import com.mxblog.pojo.Shield;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShieldServiceImpl implements ShieldService{
    @Autowired
    private ShieldMapper mapper;
    //按照id查询是否屏蔽
    public Shield queryShieldById(int id){
        return mapper.queryShieldById(id);
    }
}
