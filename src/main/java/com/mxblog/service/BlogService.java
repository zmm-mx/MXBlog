package com.mxblog.service;

import com.mxblog.pojo.Blog;

import java.util.List;
import java.util.Map;

public interface BlogService {
    //分页查询博客
    public List<Blog> queryBlogByPage(Map<String, Object> pageInfo);
    //统计博客总量
    public int countBlog(Map<String, Object> numInfo);
    //按照id查询单个博客
    public Blog queryBlogById(int id);
    //添加一个博客
    public int insertBlog(Blog blog);
    //添加博客与标签的联系
    public int insertBlogAndTag(Blog blog);
    //删除博客与标签的联系
    public int deleteBlogAndTagById(int id);
    //根据id删除一个博客
    public int deleteBlogById(int id);
    //统计当前博客是第几条
    public int countBlogById(int id);
    //修改一个博客
    public int updateBlog(Blog blog);
    //根据id屏蔽/解除一个博客
    public int shieldBlog(Map<String, Integer> shieldInfo);
    //查询每个专栏的博客数量
    public int queryBlogSumByColumn(int columnId);
    //查询每个标签的博客数量
    public int queryBlogSumByTag(int tagId);
    //查询热度最高的前十个博客
    public List<Blog> getBlogsByViews(Map<String, Object> pageInfo);
    //博客浏览量+1
    public int updateBlogViews(int id);
    //根据专栏id获取分页后未被屏蔽的博客列表
    public List<Blog> getBlogsByPageWithColumnIdWithoutShield(Map<String, Object> map);
    //根据专栏id统计未被屏蔽的博客的数量
    public int countBlogWithColumnIdWithoutShield(int id);
    //查询每个专栏下未被屏蔽的博客数量
    public int queryBlogSumByColumnIdWithoutShield(int columnId);
    //获取分页后的未被屏蔽的博客列表
    public List<Blog> queryBlogByPageWithoutShield(Map<String, Object> map);
    //获取未被屏蔽的博客总数
    public int countBlogWithoutShield();
    //根据热度获取前十个未被屏蔽的博客
    public List<Blog> getBlogsByViewsWithoutShield(Map<String, Object> pageInfo);
    //查询每个标签下未被屏蔽的博客数量
    public int queryBlogSumByTagIdWithoutShield(int tagId);
    //根据标签id获取分页后未被屏蔽的博客列表
    public List<Blog> getBlogsByPageWithTagIdWithoutShield(Map<String, Object> map);
    //查询所有博客
    public List<Blog> queryAllBlogs();
    //查询所有博客的Id
    public List<Blog> queryAllBlogIds();
    //查询所有博客的id和view
    public List<Blog> queryAllBlogIdsAndViews();
    //查询置顶博客
    public List<Blog> getTopBlogWithoutShield();
    //获取分页后未被屏蔽且没有被置顶的博客列表
    public List<Blog> queryBlogByPageWithoutShieldAndNotTop(Map<String, Object> map);
    //根据博客id判断是否被屏蔽
    public int queryShieldByBlogId(int id);
    //根据博客id获取博客的标题和id
    public Blog queryBlogTitleAndIdByBlogId(int id);
    //更新浏览量为Redis里的值
    public int updateBlogViewsWithRedis(Map<String, Object> map);
}
