package com.mxblog.service;

import com.mxblog.pojo.StemAndBranch;
import com.mxblog.pojo.Tone;
import com.mxblog.pojo.Trigram;
import com.mxblog.pojo.Yao;

import java.io.UnsupportedEncodingException;
import java.util.List;

public interface MetaphysicsService {
	String generateCode(String str);
	String parseCode(String code);
	Tone getToneByStemAndBranch(StemAndBranch stemAndBranch);
	String[] getTenGodsByStemAndBranches(StemAndBranch[] stemAndBranches);
	Trigram getTrigramByYaos(Yao[] yaos);
}
