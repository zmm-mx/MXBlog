package com.mxblog.service;

import com.mxblog.mapper.BlogTagMapper;
import com.mxblog.pojo.Blog;
import com.mxblog.pojo.BlogTag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class BlogTagServiceImpl implements BlogTagService{
    @Autowired
    private BlogTagMapper mapper;
    //分页查询博客标签
    @Override
    public List<BlogTag> queryBlogTagByPage(Map<String, Integer> pageInfo) {
        return mapper.queryBlogTagByPage(pageInfo);
    }
    //查询所有博客标签
    @Override
    public List<BlogTag> queryAllBlogTags() {
        return mapper.queryAllBlogTags();
    }
    //统计标签总量
    @Override
    public int countBlogTag() {
        return mapper.countBlogTag();
    }
    //按照id统计标签数量
    @Override
    public int countBlogTagById(int id) {
        return mapper.countBlogTagById(id);
    }
    //按照名称查询标签
    @Override
    public BlogTag queryBlogTagByName(String name) {
        return mapper.queryBlogTagByName(name);
    }
    //按照博客id查询其所有标签
    @Override
    public List<BlogTag> queryBlogTagByBlogId(int id) {
        return mapper.queryBlogTagByBlogId(id);
    }
    //按照id查询单个标签
    @Override
    public BlogTag queryBlogTagById(int id) {
        return mapper.queryBlogTagById(id);
    }
    //添加一个标签
    @Override
    public int insertBlogTag(BlogTag blogTag) {
        return mapper.insertBlogTag(blogTag);
    }
    //根据id删除一个标签
    @Override
    public int deleteBlogTagById(int id) {
        return mapper.deleteBlogTagById(id);
    }
    //修改一个标签
    @Override
    public int updateBlogTag(BlogTag blogTag) {
        return mapper.updateBlogTag(blogTag);
    }

    //把博客列表转化成编号字符串
    public String getIdList(List<BlogTag> tagList){
        StringBuffer idList = new StringBuffer();
        for (int i=0;i<tagList.size()-1;i++){
            idList.append(tagList.get(i).getId());
            idList.append(",");
        }
        idList.append(tagList.get(tagList.size()-1).getId());
        return String.valueOf(idList);
    }
}
