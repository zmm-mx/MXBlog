package com.mxblog.service;

import com.mxblog.pojo.FourPillars;
import com.mxblog.pojo.StemAndBranch;
import com.mxblog.pojo.Tone;
import com.mxblog.utils.DateStemBranchUtils;
import com.mxblog.vo.FourPillarsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FourPillarsServiceImpl implements FourPillarsService{
	@Autowired
	private MetaphysicsService metaphysicsService;

	@Override
	public FourPillarsVO getFourPillarsVO(String dateString) {
		// 创建与前端交互的 VO 数据
		FourPillarsVO vo = new FourPillarsVO();
		// 把日期和时间封装进 VO 数据
		String[] dateAndTime = dateString.split(" ");
		vo.setDate(dateAndTime[0]);
		vo.setTime(dateAndTime[1].substring(0, dateAndTime[1].length() - 3));
		// 创建四柱
		FourPillars fourPillars = new FourPillars();
		// 设置四柱的天干地支
		StemAndBranch[] stemAndBranches = DateStemBranchUtils.calStemBranch(dateString);
		fourPillars.setStemAndBranches(stemAndBranches);
		// 设置四柱的纳音
		Tone[] tones = new Tone[4];
		for (int i = 0; i < stemAndBranches.length; i++) {
			Tone tone = metaphysicsService.getToneByStemAndBranch(stemAndBranches[i]);
			// 去掉纳音的最后一个字
			tone.setName(tone.getName().substring(0, 2));
			tones[i] = tone;
		}
		fourPillars.setTones(tones);
		// 设置四柱的十神
		String[] tenGods = metaphysicsService.getTenGodsByStemAndBranches(stemAndBranches);
		fourPillars.setTenGods(tenGods);
		// 把四柱封装成进 VO 数据
		vo.setFourPillars(fourPillars);
		// 根据时间生成编码
		String code = getCode(dateString);
		// 把编码封装进 VO 数据离
		vo.setCode(code);
		return vo;
	}

	@Override
	public String getCode(String dateString) {
		return "FP-" + metaphysicsService.generateCode(dateString);
	}

	@Override
	public String subCode(String code) {
		return code.substring(3);
	}

	@Override
	public String getDateAndTime(String code) {
		return metaphysicsService.parseCode(code);
	}
}
