package com.mxblog.service;

import com.mxblog.mapper.UserMapper;
import com.mxblog.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;

    //通过用户名查询用户
    @Override
    public User queryUserByUsername(String username) {
        return userMapper.queryUserByUsername(username);
    }
    //通过ID查询用户
    @Override
    public User queryUserById(int id) {
        return userMapper.queryUserById(id);
    }
    //新增用户
    @Override
    public int insertUser(User user) {
        return userMapper.insertUser(user);
    }
    //获取最大id
    @Override
    public int getMaxId() {
        return userMapper.getMaxId();
    }
    @Override
    //通过邮箱查找用户
    public User queryUserByEmail(String email){
        return userMapper.queryUserByEmail(email);
    }
    @Override
    //分页查找用户
    public List<User> queryUserByPage(Map<String, Object> map){
        return userMapper.queryUserByPage(map);
    }
    @Override
    //统计用户总量
    public int countUsers(){
        return userMapper.countUsers();
    }
    @Override
    //根据id清除用户名
    public int clearUsernameById(Map<String, Object> map){
        return userMapper.clearUsernameById(map);
    }
    @Override
    //根据id修改用户的头像类型为jpg
    public int changeUserAvatarTypeToJPG(int id){
        return userMapper.changeUserAvatarTypeToJPG(id);
    }
}
