package com.mxblog.service;

import com.mxblog.pojo.Element;
import com.mxblog.pojo.Trigram;
import com.mxblog.vo.PlumBlossomVO;

import java.io.UnsupportedEncodingException;
import java.util.List;

public interface PlumBlossomService {
	PlumBlossomVO getPlumBlossomVO(String ch1, String ch2, String dateString) throws UnsupportedEncodingException;
	Trigram calculateTrigram(int num);
	Trigram getTrigramWithChanging(Trigram origin, int changing);
	String getLuck(Trigram body, Trigram using);
	String getSituation(Element bodyElement, Element monthElement);
	String getCode(String ch1, String ch2, String dateString) throws UnsupportedEncodingException;
	String subCode(String code);
	List<String> getCharactersAndTime(String code) throws UnsupportedEncodingException;
}
