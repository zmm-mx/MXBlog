package com.mxblog.service;

import com.mxblog.pojo.Blog;

import java.util.List;

public interface SearchService {
    // 创建索引
    void createIndex();
    // 把博客对象存入ES中
    void insertBlog(Blog blog);
    // 批量把博客对象存入ES中
    void insertBlogList(List<Blog> blogList);
    // 分页查询博客
    List<Blog> findBlogByTitleWithPage(String title, int from, int size);
}
