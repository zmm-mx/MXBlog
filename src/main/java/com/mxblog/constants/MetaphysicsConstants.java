package com.mxblog.constants;

import com.mxblog.pojo.*;

/**
 * 梅花易数相关的常量
 * @Author 妙霄
 */
public class MetaphysicsConstants {
	public static final YinYang YIN = new YinYang("阴");
	public static final YinYang YANG = new YinYang("阳");

	public static final Yao YANG_YAO = new Yao("▄▄▄▄▄▄", "█████████", YANG);
	public static final Yao YIN_YAO = new Yao("▄▄<font color=\"#FFF\">▄▄</font>▄▄", "███<font color=\"#FFF\">███</font>███", YIN);

	public static final Element EL_METAL = new Element(1, "金", 3, 2, "white");
	public static final Element EL_WOOD = new Element(2, "木", 4, 5, "green");
	public static final Element EL_WATER = new Element(3, "水", 2, 4, "black");
	public static final Element EL_FIRE = new Element(4, "火", 5, 1, "red");
	public static final Element EL_EARTH = new Element(5, "土", 1, 3, "yellow");

	public static final Trigram TR_HEAVEN = new Trigram(1, "乾", new Yao[]{YANG_YAO, YANG_YAO, YANG_YAO}, EL_METAL);
	public static final Trigram TR_LAKE = new Trigram(2, "兑", new Yao[]{YIN_YAO, YANG_YAO, YANG_YAO}, EL_METAL);
	public static final Trigram TR_FIRE = new Trigram(3, "离", new Yao[]{YANG_YAO, YIN_YAO, YANG_YAO}, EL_FIRE);
	public static final Trigram TR_THUNDER = new Trigram(4, "震", new Yao[]{YIN_YAO, YIN_YAO, YANG_YAO}, EL_WOOD);
	public static final Trigram TR_WIND = new Trigram(5, "巽", new Yao[]{YANG_YAO, YANG_YAO, YIN_YAO}, EL_WOOD);
	public static final Trigram TR_WATER = new Trigram(6, "坎", new Yao[]{YIN_YAO, YANG_YAO, YIN_YAO}, EL_WATER);
	public static final Trigram TR_MOUNTAIN = new Trigram(7, "艮", new Yao[]{YANG_YAO, YIN_YAO, YIN_YAO}, EL_EARTH);
	public static final Trigram TR_EARTH = new Trigram(8, "坤", new Yao[]{YIN_YAO, YIN_YAO, YIN_YAO}, EL_EARTH);
	public static final Trigram[] TRIGRAM_LIST = new Trigram[]{TR_HEAVEN, TR_LAKE, TR_FIRE, TR_THUNDER, TR_WIND, TR_WATER, TR_MOUNTAIN, TR_EARTH};

	public static final Stem ST_JIA = new Stem(1, "甲", EL_WOOD, YANG);
	public static final Stem ST_YI = new Stem(2, "乙", EL_WOOD, YIN);
	public static final Stem ST_BING = new Stem(3, "丙", EL_FIRE, YANG);
	public static final Stem ST_DING = new Stem(4, "丁", EL_FIRE, YIN);
	public static final Stem ST_WU = new Stem(5, "戊", EL_EARTH, YANG);
	public static final Stem ST_JI = new Stem(6, "己", EL_EARTH, YIN);
	public static final Stem ST_GENG = new Stem(7, "庚", EL_METAL, YANG);
	public static final Stem ST_XIN = new Stem(8, "辛", EL_METAL, YIN);
	public static final Stem ST_REN = new Stem(9, "壬", EL_WATER, YANG);
	public static final Stem ST_GUI = new Stem(10, "癸", EL_WATER, YIN);
	public static final Stem[] STEM_LIST = new Stem[]{ST_JIA, ST_YI, ST_BING, ST_DING, ST_WU, ST_JI, ST_GENG, ST_XIN, ST_REN, ST_GUI};

	public static final Branch BR_ZI = new Branch(1, "子", "鼠", EL_WATER);
	public static final Branch BR_CHOU = new Branch(2, "丑", "牛", EL_EARTH);
	public static final Branch BR_YIN = new Branch(3, "寅", "虎", EL_WOOD);
	public static final Branch BR_MAO = new Branch(4, "卯", "兔", EL_WOOD);
	public static final Branch BR_CHEN = new Branch(5, "辰", "龙", EL_EARTH);
	public static final Branch BR_SI = new Branch(6, "巳", "蛇", EL_FIRE);
	public static final Branch BR_WU = new Branch(7, "午", "马", EL_FIRE);
	public static final Branch BR_WEI = new Branch(8, "未", "羊", EL_EARTH);
	public static final Branch BR_SHEN = new Branch(9, "申", "猴", EL_METAL);
	public static final Branch BR_YOU = new Branch(10, "酉", "鸡", EL_METAL);
	public static final Branch BR_XU = new Branch(11, "戌", "狗", EL_EARTH);
	public static final Branch BR_HAI = new Branch(12, "亥", "猪", EL_WATER);
	public static final Branch[] BRANCH_LIST = new Branch[]{BR_ZI, BR_CHOU, BR_YIN, BR_MAO, BR_CHEN, BR_SI, BR_WU, BR_WEI, BR_SHEN, BR_YOU, BR_XU, BR_HAI};

	public static final String[] SIX_GOD_LIST = new String[]{"青龙", "朱雀", "勾陈", "螣蛇", "白虎", "玄武"};

	public static final StemAndBranch SB_JIA_ZI = new StemAndBranch(ST_JIA, BR_ZI);
	public static final StemAndBranch SB_YI_CHOU = new StemAndBranch(ST_YI, BR_CHOU);
	public static final StemAndBranch SB_BING_YIN = new StemAndBranch(ST_BING, BR_YIN);
	public static final StemAndBranch SB_DING_MAO = new StemAndBranch(ST_DING, BR_MAO);
	public static final StemAndBranch SB_WU_CHEN = new StemAndBranch(ST_WU, BR_CHEN);
	public static final StemAndBranch SB_JI_SI = new StemAndBranch(ST_JI, BR_SI);
	public static final StemAndBranch SB_GENG_WU = new StemAndBranch(ST_GENG, BR_WU);
	public static final StemAndBranch SB_XIN_WEI = new StemAndBranch(ST_XIN, BR_WEI);
	public static final StemAndBranch SB_REN_SHEN = new StemAndBranch(ST_REN, BR_SHEN);
	public static final StemAndBranch SB_GUI_YOU = new StemAndBranch(ST_GUI, BR_YOU);

	public static final StemAndBranch SB_JIA_XU = new StemAndBranch(ST_JIA, BR_XU);
	public static final StemAndBranch SB_YI_HAI = new StemAndBranch(ST_YI, BR_HAI);
	public static final StemAndBranch SB_BING_ZI = new StemAndBranch(ST_BING, BR_ZI);
	public static final StemAndBranch SB_DING_CHOU = new StemAndBranch(ST_DING, BR_CHOU);
	public static final StemAndBranch SB_WU_YIN = new StemAndBranch(ST_WU, BR_YIN);
	public static final StemAndBranch SB_JI_MAO = new StemAndBranch(ST_JI, BR_MAO);
	public static final StemAndBranch SB_GENG_CHEN = new StemAndBranch(ST_GENG, BR_CHEN);
	public static final StemAndBranch SB_XIN_SI = new StemAndBranch(ST_XIN, BR_SI);
	public static final StemAndBranch SB_REN_WU = new StemAndBranch(ST_REN, BR_WU);
	public static final StemAndBranch SB_GUI_WEI = new StemAndBranch(ST_GUI, BR_WEI);

	public static final StemAndBranch SB_JIA_SHEN = new StemAndBranch(ST_JIA, BR_SHEN);
	public static final StemAndBranch SB_YI_YOU = new StemAndBranch(ST_YI, BR_YOU);
	public static final StemAndBranch SB_BING_XU = new StemAndBranch(ST_BING, BR_XU);
	public static final StemAndBranch SB_DING_HAI = new StemAndBranch(ST_DING, BR_HAI);
	public static final StemAndBranch SB_WU_ZI = new StemAndBranch(ST_WU, BR_ZI);
	public static final StemAndBranch SB_JI_CHOU = new StemAndBranch(ST_JI, BR_CHOU);
	public static final StemAndBranch SB_GENG_YIN = new StemAndBranch(ST_GENG, BR_YIN);
	public static final StemAndBranch SB_XIN_MAO = new StemAndBranch(ST_XIN, BR_MAO);
	public static final StemAndBranch SB_REN_CHEN = new StemAndBranch(ST_REN, BR_CHEN);
	public static final StemAndBranch SB_GUI_SI = new StemAndBranch(ST_GUI, BR_SI);

	public static final StemAndBranch SB_JIA_WU = new StemAndBranch(ST_JIA, BR_WU);
	public static final StemAndBranch SB_YI_WEI = new StemAndBranch(ST_YI, BR_WEI);
	public static final StemAndBranch SB_BING_SHEN = new StemAndBranch(ST_BING, BR_SHEN);
	public static final StemAndBranch SB_DING_YOU = new StemAndBranch(ST_DING, BR_YOU);
	public static final StemAndBranch SB_WU_XU = new StemAndBranch(ST_WU, BR_XU);
	public static final StemAndBranch SB_JI_HAI = new StemAndBranch(ST_JI, BR_HAI);
	public static final StemAndBranch SB_GENG_ZI = new StemAndBranch(ST_GENG, BR_ZI);
	public static final StemAndBranch SB_XIN_CHOU = new StemAndBranch(ST_XIN, BR_CHOU);
	public static final StemAndBranch SB_REN_YIN = new StemAndBranch(ST_REN, BR_YIN);
	public static final StemAndBranch SB_GUI_MAO = new StemAndBranch(ST_GUI, BR_MAO);

	public static final StemAndBranch SB_JIA_CHEN = new StemAndBranch(ST_JIA, BR_CHEN);
	public static final StemAndBranch SB_YI_SI = new StemAndBranch(ST_YI, BR_SI);
	public static final StemAndBranch SB_BING_WU = new StemAndBranch(ST_BING, BR_WU);
	public static final StemAndBranch SB_DING_WEI = new StemAndBranch(ST_DING, BR_WEI);
	public static final StemAndBranch SB_WU_SHEN = new StemAndBranch(ST_WU, BR_SHEN);
	public static final StemAndBranch SB_JI_YOU = new StemAndBranch(ST_JI, BR_YOU);
	public static final StemAndBranch SB_GENG_XU = new StemAndBranch(ST_GENG, BR_XU);
	public static final StemAndBranch SB_XIN_HAI = new StemAndBranch(ST_XIN, BR_HAI);
	public static final StemAndBranch SB_REN_ZI = new StemAndBranch(ST_REN, BR_ZI);
	public static final StemAndBranch SB_GUI_CHOU = new StemAndBranch(ST_GUI, BR_CHOU);

	public static final StemAndBranch SB_JIA_YIN = new StemAndBranch(ST_JIA, BR_YIN);
	public static final StemAndBranch SB_YI_MAO = new StemAndBranch(ST_YI, BR_MAO);
	public static final StemAndBranch SB_BING_CHEN = new StemAndBranch(ST_BING, BR_CHEN);
	public static final StemAndBranch SB_DING_SI = new StemAndBranch(ST_DING, BR_SI);
	public static final StemAndBranch SB_WU_WU = new StemAndBranch(ST_WU, BR_WU);
	public static final StemAndBranch SB_JI_WEI = new StemAndBranch(ST_JI, BR_WEI);
	public static final StemAndBranch SB_GENG_SHEN = new StemAndBranch(ST_GENG, BR_SHEN);
	public static final StemAndBranch SB_XIN_YOU = new StemAndBranch(ST_XIN, BR_YOU);
	public static final StemAndBranch SB_REN_XU = new StemAndBranch(ST_REN, BR_XU);
	public static final StemAndBranch SB_GUI_HAI = new StemAndBranch(ST_GUI, BR_HAI);
}
