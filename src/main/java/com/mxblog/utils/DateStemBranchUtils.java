package com.mxblog.utils;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.mxblog.constants.MetaphysicsConstants;
import com.mxblog.pojo.StemAndBranch;

import java.util.*;

/**
 * 获取阳历日期的年月日时干支 只支持1900-02-04至2050-01-22 (1900-02-04前按节气划分为1989年，2050-01-22之后为农历二〇五〇年，转阴历用的lunarInfo只到2049年)
 * 日干支参考: https://zhuanlan.zhihu.com/p/93508430
 * 年、月、时干支参考百度百科: https://baike.baidu.com/item/%E5%A4%A9%E5%B9%B2%E5%9C%B0%E6%94%AF/278140?fromModule=lemma-qiyi_sense-lemma
 *
 * @author Administrator
 */
public class DateStemBranchUtils {

	protected static final String[] HEAVENLY_STEM = {"甲", "乙", "丙", "丁", "戊", "己", "庚", "辛", "壬", "癸"};
	protected static final String[] EARTHLY_BRANCHES = {"子", "丑", "寅", "卯", "辰", "巳", "午", "未", "申", "酉", "戌", "亥"};
	/**
	 * 时辰数组
	 */
	protected static final Integer[][] HOUR_ARRAY = {{23, 1}, {1, 3}, {3, 5}, {5, 7}, {7, 9}, {9, 11}, {11, 13}, {13, 15}, {15, 17}, {17, 19}, {19, 21}, {21, 23}};

	/**
	 * 按照年份计算世纪
	 * 此方法只应用于计算世纪常数，每个00年3月后记为新世纪
	 *
	 * @param year
	 * @return
	 */
	private static int centuryFromYear(int year, int month) {
		// 每个00年3月后记为新世纪
		if (year % 100 == 0 && month < 3) {
			year = year / 100;
		} else {
			year = (year / 100) + 1;
		}
		return year;
	}

	/**
	 * 计算世纪常数
	 * (ⅰ)每世纪的第一年是从“0”开始的，故整百整千的“世纪年”即为每世纪的第一年。例如：1900年为20世纪的第1年，其公元年份后两位为零，即s=0。
	 * <p>
	 * (ⅱ)世纪常数x每世纪第一年3月1日的的日柱序列数减1。
	 * 例如:21世纪的世纪常数等于2000年3月1日的日柱(戊午)序列数减1，即x=55-1=54，同时也是-6（由于干支序列的60循环特点，而54-60=-6，在干支计算上54与-6是等价的）。
	 *
	 * @param year
	 * @return 世纪常数
	 */
	private static int getCenturyConstant(int year, int month) {
		// 世纪数 区分16世纪前后
		int x = 0;
		int n = centuryFromYear(year, month);
		if (n <= 16) {
			x = 45 * (n - 16) + 22;
		}
		if (n > 16) {
			x = 44 * (n - 17) + (n - 17) / 4 - 3;
		}
		return x;
	}

	/**
	 * 1582年10月15日—1599年14月29日”期间的日柱时，必须在算式最后再减去10 其他时间无需如此。
	 */
	public static int containsSpecialDuration(int year, int month, int day) {
		int m = month < 3 ? (month + 12) : month;
		if (year > 1600 || year < 1582) {
			return 0;
		}
		if (year == 1582 && (m * 100 + day) < 1015) {
			return 0;
		}
		if (year == 1599 && (m * 100 + day) > 1429) {
			return 0;
		}
		return 10;
	}

	/**
	 * 计算日干支数(干支表顺序)
	 *
	 * @param date
	 * @return
	 */
	public static int getStemsBranch(Date date) {
		int monthNum = DateUtil.month(date) + 1;
		int yearNum = DateUtil.year(date);
		int mon = monthNum < 3 ? (monthNum + 12) : monthNum;
		String year = String.valueOf(yearNum);
		// s：公元年数后两位数, 取整数值，month < 3时，年份-1(当成前一年算)
		int s;
		if (monthNum < 3) {
			if (yearNum % 100 == 0) {
				s = 99;
			} else {
				s = Integer.parseInt(year.substring(2)) - 1;
			}
		} else {
			s = Integer.parseInt(year.substring(2));
		}

		int sd4 = s / 4;
		// u：s除以4的余数
		int u = s % 4;
		int cal = (3 * mon - 7) / 5;
		// m：月数
		int m = (int) (30 * ((Math.pow(-1, mon) + 1) / 2) + cal);
		// d：日期数
		int d = DateUtil.dayOfMonth(date);
		// x：世纪常数
		int x = getCenturyConstant(yearNum, monthNum) % 60;

		// 在计算“1582年10月15日—1599年14月29日”期间的日柱时，必须在算式最后再减去10
		int specialDuration = containsSpecialDuration(yearNum, monthNum, d);

		// r 日柱的母数，r 除以60的余数即是日柱的干支序列数 公式：r=s/4×6+5(s/4×3+u)+m+d+x
		int r = (sd4 * 6) + (5 * (sd4 * 3 + u)) + m + d + x - specialDuration;
		int res = r % 60;
		return res == 0 ? 60 : res;
	}

	/**
	 * 计算年干支(公元后)
	 * 干：甲=4…… 支：子=4……
	 *
	 * @param year 阴历年份
	 * @return 年干支
	 */
	public static StemAndBranch calYearStemBranch(int year) {
		String strYear = String.valueOf(year);
		int lastDigitOfYear = Integer.parseInt(strYear.substring(strYear.length() - 1));
		// 按照天干下标从0开始，年份最后一位(<=3时借十取最后一位) - 4
		lastDigitOfYear = lastDigitOfYear <= 3 ? lastDigitOfYear + 10 - 4 : lastDigitOfYear - 4;
		String heavenlyStem = HEAVENLY_STEM[lastDigitOfYear];
		// 按照地支下标从0开始，(年份 + 8) % 12
		int remainder = (year + 8) % 12;
		String earthlyBranch = EARTHLY_BRANCHES[remainder];
		// 生肖
		String animal = LunarCalendarFestivalUtils.animals[(year - 4) % 12];
//		return heavenlyStem + earthlyBranch + animal + "年";
		return new StemAndBranch(MetaphysicsConstants.STEM_LIST[lastDigitOfYear], MetaphysicsConstants.BRANCH_LIST[remainder]);
	}

	/**
	 * 计算月干支
	 * 年干：甲乙丙丁……的顺序 1-10 下标0-9
	 * 月支：寅卯……子丑的顺序(就是月份数) 1-12 下标0-11
	 * 月干 = 年干*2+月支 大于十取个位 甲乙丙丁……的顺序 1-10 下标0-9
	 *
	 * @param year  阴历年份 年份也是节气划分
	 * @param month 阴历月份 是以节气划分的月份，新年不是从初一开始，从立春开始
	 * @return 月干支
	 */
	public static StemAndBranch calMonthStemBranch(int year, int month) {
		String strYear = String.valueOf(year);
		// 年干数
		int lastDigitOfYear = Integer.parseInt(strYear.substring(strYear.length() - 1));
		lastDigitOfYear = lastDigitOfYear <= 3 ? lastDigitOfYear + 10 - 3 : lastDigitOfYear - 3;
		// 月干
		int monthStemNum = lastDigitOfYear * 2 + month;
		// 月干数最后一位
		int lastDigitOfMonth = Integer.parseInt(String.valueOf(monthStemNum).substring(String.valueOf(monthStemNum).length() - 1)) - 1;
		// 月干数组下标，如果最后一位是0 - 1，说明是癸，癸的下标是9，直接取九 否则会数组下标越界
		lastDigitOfMonth = lastDigitOfMonth < 0 ? 9 : lastDigitOfMonth;
		// 获取月支数组下标
		int monthBranchIndex = month >= 11 ? month - 11 : month + 1;
//		return HEAVENLY_STEM[lastDigitOfMonth] + EARTHLY_BRANCHES[monthBranchIndex] + "月";
		return new StemAndBranch(MetaphysicsConstants.STEM_LIST[lastDigitOfMonth], MetaphysicsConstants.BRANCH_LIST[monthBranchIndex]);
	}

	/**
	 * 计算日干支
	 *
	 * @param date 阳历日期
	 * @return 日干支
	 */
	public static StemAndBranch calDayStemBranch(String date) {
		// 获取日干支的干支表顺序
		int dayIndex = getStemsBranch(DateUtil.parse(date, DatePattern.NORM_DATE_PATTERN));
		/*
		List<String> list = new ArrayList<>();
		int branchIndex = 0;
		// 组装干支表
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < HEAVENLY_STEM.length; j++) {
				if ((i * 10 + j) % 12 == 0) {
					branchIndex = 0;
				}
				list.add(HEAVENLY_STEM[j] + EARTHLY_BRANCHES[branchIndex]);
				branchIndex++;
			}
		}
		String[] stemBranches = list.toArray(new String[0]);
		*/
//		return stemBranches[dayIndex - 1] + "日";
		int stemIndex = ((stemIndex = dayIndex % 10) == 0 ? 10 : stemIndex) - 1;
		int branchIndex = ((branchIndex = dayIndex % 12) == 0 ? 12 : branchIndex) - 1;
		return new StemAndBranch(MetaphysicsConstants.STEM_LIST[stemIndex], MetaphysicsConstants.BRANCH_LIST[branchIndex]);
	}

	/**
	 * 计算时干支
	 *
	 * @param hour    24小时制的小时数
	 * @param dayStem 日干支下标
	 * @return 时干支
	 */
	public static StemAndBranch calHourStemBranch(Integer hour, int dayStem) {
		int branchIndex = 0;
		// 获取时辰地支 从丑时-亥时，没有筛选到默认为子时
		for (int i = 1; i < HOUR_ARRAY.length; i++) {
			if (hour >= HOUR_ARRAY[i][0] && hour < HOUR_ARRAY[i][1]) {
				branchIndex = i;
			}
		}
		// 子时干支
		int zeroStemIndex = 0;
		// 甲己还加甲，乙庚丙作初；丙辛从戊起，丁壬庚子居；戊癸何方发，壬子是真途
		switch (dayStem) {
			case 1:
			case 6:
				zeroStemIndex = 2;
				break;
			case 2:
			case 7:
				zeroStemIndex = 4;
				break;
			case 3:
			case 8:
				zeroStemIndex = 6;
				break;
			case 4:
			case 9:
				zeroStemIndex = 8;
				break;
			default:
		}
		// 如果是23时计算为下一天
		boolean nextDay = hour >= 23;
		int stemIndex = nextDay ? zeroStemIndex + branchIndex + 2 : zeroStemIndex + branchIndex;
		// 时干下标
		stemIndex = stemIndex > 9 ? stemIndex - 10 : stemIndex;
//		return HEAVENLY_STEM[stemIndex] + EARTHLY_BRANCHES[branchIndex] + "时";
		return new StemAndBranch(MetaphysicsConstants.STEM_LIST[stemIndex], MetaphysicsConstants.BRANCH_LIST[branchIndex]);
	}

	/**
	 * 阳历日期计算 年月日时干支
	 * 年用阴历年份算，正月初一为一年开始
	 * 月用节气划分的月算，立春为一年开始
	 * 日用阳历算
	 *
	 * @param dateTime yyyy-MM-dd HH:mm:ss
	 * @return 阴历日期 + 年干支 + 生肖 + 月日时干支
	 */
	public static StemAndBranch[] calStemBranch(String dateTime) {
		DateTime parse = DateUtil.parse(dateTime, DatePattern.NORM_DATETIME_PATTERN);
		String date = DateUtil.format(parse, DatePattern.NORM_DATE_PATTERN);
		// 获取阴历日期(yyyy-MM-dd) 阴历会有二月三十，获取不能转阳历格式，用split代替
		String lunarCalendar = LunarCalendarFestivalUtils.toLunarCalendar(date);
		// 阴历日期(一九九六年 五月三十)
		String calendar = LunarCalendarFestivalUtils.getLunarCalendar(date);
		// 阴历年份
		int year = Integer.parseInt(lunarCalendar.split("-")[0]);
		// 年干支
		StemAndBranch yearStemBranch = calYearStemBranch(year);
		// 计算干支纪月的年、月按照节气算，立春开始算正月，两个节气一个月
		int lunarTermYear = LunarCalendarFestivalUtils.getLunarTermYear(date);
		int lunarTermMonth = LunarCalendarFestivalUtils.getLunarTermMonth(date);
		// 月干支
		StemAndBranch monthStemBranch = calMonthStemBranch(lunarTermYear, lunarTermMonth);
		// 日干支
		StemAndBranch dayStemBranch = calDayStemBranch(date);
		// 获取日干下标
//		int dayStem = LunarCalendarFestivalUtils.printArray(HEAVENLY_STEM, dayStemBranch.substring(0, 1));
		int dayStem = dayStemBranch.getStem().getId() - 1;
		// 计算时干支
		StemAndBranch hourStemBranch = calHourStemBranch(DateUtil.hour(parse, true), dayStem);
//		return calendar + " " + yearStemBranch + monthStemBranch + dayStemBranch + " " + hourStemBranch;
		return new StemAndBranch[]{yearStemBranch, monthStemBranch, dayStemBranch, hourStemBranch};
	}
}
