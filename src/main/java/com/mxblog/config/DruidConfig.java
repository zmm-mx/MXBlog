package com.mxblog.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.alibaba.druid.wall.WallConfig;
import com.alibaba.druid.wall.WallFilter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Configuration
public class DruidConfig {
    /*
       将自定义的 Druid数据源添加到容器中，不再让 Spring Boot 自动创建
       绑定全局配置文件中的 druid 数据源属性到 com.alibaba.druid.pool.DruidDataSource 从而让它们生效
    */
    @ConfigurationProperties(prefix = "spring.datasource")
    @Bean
    public DataSource druidDataSource(){
        //如果不需要执行多条sql，则只需要这一句即可：
        //return new DruidDataSource();
        //如果需要执行多条sql，则需要配置WallFilter和WallConfig
        List filterList=new ArrayList<>(); //可以链式叠加
        filterList.add(wallFilter());
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setProxyFilters(filterList);
        return druidDataSource;
    }

    /**
     * 配置druid执行多条sql（批量执行），避免报sql注入异常
     * 链式配置,从下往上注入
     * @return
     */
    @Bean
    public WallFilter wallFilter() {
        WallFilter wallFilter = new WallFilter();
        wallFilter.setConfig(wallConfig());
        return wallFilter;
    }
    @Bean
    public WallConfig wallConfig() {
        WallConfig wallconfig = new WallConfig();
        wallconfig .setMultiStatementAllow(true);//允许一次执行多条语句
        wallconfig .setNoneBaseStatementAllow(true);//允许非基本语句的其他语句
        return wallconfig ;
    }

    //后台监控 相当于web.xml
    //由于SpringBoot内置了Servlet，所以没有web.xml
    //因此当我们想要使用web.xml时，就需要注册ServletRegistrationBean
    @Bean
    public ServletRegistrationBean statViewServlet(){
        ServletRegistrationBean<StatViewServlet> bean = new ServletRegistrationBean<>(new StatViewServlet(), "/druid/*");
        //后台需要有人登录，账号密码配置
        HashMap<String, String> initParameters = new HashMap<>();
        //增加配置
        //登录的用户名和密码的Key是固定的，即loginUsername和loginPassword，不可以更换
        initParameters.put("loginUsername", "root");
        initParameters.put("loginPassword", "123456");
        //允许谁可以访问
        initParameters.put("allow",""); //value为空意味着所有人都能访问
        //设置初始化参数
        bean.setInitParameters(initParameters);
        return bean;
    }

    //filter 过滤器
    @Bean
    public FilterRegistrationBean webStatFilter(){
        FilterRegistrationBean bean = new FilterRegistrationBean();
        bean.setFilter(new WebStatFilter());
        //配置可以过滤的请求
        HashMap<String, String> initParameters = new HashMap<>();
        //exclusions：不参与统计
        initParameters.put("exclusions", "*.js,*.css,/druid/*");
        bean.setInitParameters(initParameters);
        return bean;
    }
}