package com.mxblog.config;

import com.mxblog.pojo.User;
import com.mxblog.service.UserServiceImpl;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

//自定义的Realm
public class UserRealm extends AuthorizingRealm {
    @Autowired
    UserServiceImpl userService;

    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        //拿到当前登录的对象
        Subject subject = SecurityUtils.getSubject();
        User currentUser = (User) subject.getPrincipal();
        //设置当前用户的权限为数据库中的权限
        String[] auths = currentUser.getAuthority().split(",");
        for (String auth : auths) {
            info.addStringPermission(auth);
        }
        return info;
    }
    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        //邮箱、密码从数据库中获取
        User user = userService.queryUserByEmail(token.getUsername());
        //user==null说明没有找到
        if (user==null){
            //抛出UnknownAccountException异常，即用户名不存在的异常
            return null;
        }else{
            //盐值加密
            ByteSource credentialsSalt = ByteSource.Util.bytes(user.getUsername());
            //IncorrectCredentialsException异常，即密码错误的异常交给Shiro来检测
            return new SimpleAuthenticationInfo(user, user.getPassword(), credentialsSalt, getName());
        }
    }
}