package com.mxblog;

import com.mxblog.controller.BlogRouteController;
import com.mxblog.service.*;
import com.mxblog.utils.RedisUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MxblogApplicationTests {

    @Autowired
    UserServiceImpl userService;
    @Autowired
    BlogServiceImpl blogService;
    @Autowired
    RedisUtil redisUtil;
    /* 跟随 ES 测试代码一起被注释
    @Autowired
    SearchServiceImpl searchService;
    */
    @Autowired
    GameService gameService;
    @Autowired
    BlogRouteController blogRouteController;
    @Autowired
    PlumBlossomService plumBlossomService;

    @Test
    void test() {
        /*User user = userService.queryUserByUsername("妙霄");
        System.out.println(user);*/

        /*redisUtil.set("testKey", "中文测试结果");
        System.out.println(redisUtil.get("testKey"));
        System.out.println(redisUtil.get("testKey"));
        System.out.println(redisUtil.get("testKey"));
        System.out.println(redisUtil.get("testKey"));
        System.out.println(redisUtil.get("testKey"));
        redisUtil.set("user", userService.queryUserByUsername("妙霄"));
        System.out.println(redisUtil.get("user"));
        System.out.println(redisUtil.keys("*"));*/

        /*List<Blog> blogs = blogService.queryAllBlogIdsAndViews();*/
        //初始化view为sql中的数据
        /*for (Blog blog : blogs) {
            redisUtil.set(blog.getRedisKeyWithId(), blog.getViews());
        }*/

        //把空score设置为0
        /*for (Blog blog : blogs) {
            System.out.println(blog.getRedisKeyWithId() + "\t" + redisUtil.get(blog.getRedisKeyWithId()));
            redisUtil.ZIncrBy("mxblog:rank:view", blog.getId(), 0);
        }
        List<Blog> blogIds = blogService.queryAllBlogIds();
        for (Blog blog : blogIds) {
            redisUtil.ZAdd("mxblog:rank:view", blog.getId(), 0);
        }*/

        /*System.out.println("=====================");
        Set<ZSetOperations.TypedTuple<Object>> range = redisUtil.ZRevRangeWithScore("mxblog:rank:view", 0, 9);
        for (ZSetOperations.TypedTuple<Object> idWithScore : range) {
            System.out.println("mxblog:view:blog:" + idWithScore.getValue() + "\t" + redisUtil.get("mxblog:view:blog:" + idWithScore.getValue()) + "\t" + idWithScore.getScore());
        }
        System.out.println("=====================");
        System.out.println("未被屏蔽的博客id列表如下：");
        int pageSize = 10;
        Set newSet = new LinkedHashSet();
        for(int i = pageSize, last = -1;i>0;) {
            if (last>=redisUtil.ZCard("mxblog:rank:view")) {
                break;
            }
            Set set = redisUtil.ZRevRange("mxblog:rank:view", last+1, last+pageSize);
            Set removeSet = new LinkedHashSet();
            for (Object id : set) {
                if (i<=0) {
                    removeSet.add(id);
                }
                else {
                    if (blogService.queryShieldByBlogId((int)id)==-1) {
                        removeSet.add(id);
                    } else {
                        i--;
                    }
                }
            }
            set.removeAll(removeSet);
            newSet.addAll(set);
            removeSet.clear();
            last = last + pageSize;
        }
        ArrayList<Blog> blogList = new ArrayList<>();
        for (Object id : newSet) {
            blogList.add(blogService.queryBlogTitleAndIdByBlogId((int)id));
        }
        for (Blog blog : blogList) {
            System.out.println(blog);
        }*/

        /* 原本用于 ES 测试的代码
        System.setProperty("es.set.netty.runtime.available.processors","false");
        List<Blog> allBlogs = blogService.queryAllBlogs();
        searchService.createIndex();
        searchService.insertBlogList(allBlogs);
        List<Blog> blogList = searchService.findBlogByTitleWithPage("Sp", 0, 10);
        for (Blog blog : blogList) {
            System.out.println(blog);
        }
        */

//        System.out.println(blogRouteController.getBlogs(null, null, "1"));

        /*System.out.println(gameService.queryCountsOfGames());
        System.out.println(gameService.queryAllGames());*/

        /*System.out.println(PlumBlossomConstants.YANG_YAO);
        System.out.println(PlumBlossomConstants.YIN_YAO);*/

        /*try {
            System.out.println(CharacterUtils.calculateNumberOfStrokes("金"));
            System.out.println(CharacterUtils.calculateNumberOfStrokes("木"));
            System.out.println(CharacterUtils.calculateNumberOfStrokes("水"));
            System.out.println(CharacterUtils.calculateNumberOfStrokes("火"));
            System.out.println(CharacterUtils.calculateNumberOfStrokes("土"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }*/

        /*
        try {
            System.out.println(plumBlossomService.calculateTrigram(CharacterUtils.calculateNumberOfStrokes("快")));
            System.out.println(plumBlossomService.calculateTrigram(CharacterUtils.calculateNumberOfStrokes("乐")));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        */

        /*
        StemAndBranch[] stemBranchs = calStemBranch("2023-07-16 11:23:45");
        System.out.println(Arrays.toString(stemBranchs));   // 二零二三年 五月廿九 癸卯兔年己未月乙亥日壬午时
        */

       /* try {
//        DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String dateString = dateformat.format(new Date());
            String dateString = "2020-11-11 10:42:45";
            PlumBlossomVO vo = plumBlossomService.getPlumBlossomVO("快", "乐", dateString);
            System.out.println(JSON.toJSONString(vo, SerializerFeature.DisableCircularReferenceDetect));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }*/

        /*try {
            byte[] bytes1 = "快".getBytes("GBK");
            byte[] bytes2 = "乐".getBytes("GBK");
            StringBuilder sb = new StringBuilder();
            sb.append(bytes1[0]);
            sb.append("T");
            sb.append(bytes1[1]);
            sb.append("K");
            sb.append(bytes2[0]);
            sb.append("T");
            sb.append(bytes2[1]);
            sb.append("K");
            String dateString = "2020-11-11 10:42:45";
            sb.append(dateString);
            StringBuilder res = new StringBuilder();
            for (int i = 0; i < sb.length(); i++) {
                char c = sb.charAt(i);
                if (c == '-') {
                    res.append("M");
                }
                else if (c == ':') {
                    res.append("O");
                }
                else if (c == ' ') {
                    res.append("S");
                }
                else if (c >= '0' && c <= '9') {
                    res.append((char) (c + 17));
                }
                else {
                    res.append(c);
                }
            }
            System.out.println(sb.toString());      // -65T-20K-64T-42K2020-11-11 10:42:45
            System.out.println(res.toString());     // MGFTMCAKMGETMECKCACAMBBMBBSBAOECOEF
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }*/

        /*try {
            String code = plumBlossomService.getCode("快", "乐", "2020-11-11 10:42:45");
            System.out.println(code);
            System.out.println(code.equals("MGFTMCAKMGETMECKCACAMBBMBBSBAOECOEF"));
            List<String> list = plumBlossomService.splitCode("MGFTMCAKMGETMECKCACAMBBMBBSBAOECOEF");
            System.out.println(list);
            System.out.println(list.get(0).equals("快"));
            System.out.println(list.get(1).equals("乐"));
            System.out.println(list.get(2).equals("2020-11-11 10:42:45"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }*/

        char result1 = (char) (0x4e00 + (int) (Math.random() * (0x9fa5 - 0x4e00 + 1)));
        char result2 = (char) (0x4e00 + (int) (Math.random() * (0x9fa5 - 0x4e00 + 1)));
        System.out.println(result1);
        System.out.println(result2);

    }

}
