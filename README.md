# 妙霄的博客

#### 介绍
妙霄的博客（个人博客）  
点击访问：http://www.mxblog.top

玄学小站（玄学知识与工具）  
点击访问：http://www.mxblog.top/metaphysics

#### 架构
- 前端：HTML + CSS + JS + Semantic UI + Thymeleaf  
- 后端：Spring Boot + MyBatis + Redis